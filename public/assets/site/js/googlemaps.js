$(document).ready(function(){
    var placeSearch, autocomplete, autocomplete2;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {

        var southWest = new google.maps.LatLng(34.5428, -31.4647999);
        var northEast = new google.maps.LatLng(82.1673907, 74.3555001);
        var myCustomEuropeBounds = new google.maps.LatLngBounds(southWest, northEast);
        var options = {
            bounds: myCustomEuropeBounds,
            strictBounds: true,
            types: ['(cities)']
        };

        function autocomplete() {
            new google.maps.places.Autocomplete(document.getElementById('autocomplete'), options);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                fillInAddress();
            });
        }

        function autocomplete2() {
            new google.maps.places.Autocomplete(document.getElementById('autocomplete2'), options);

            google.maps.event.addListener(autocomplete2, 'place_changed', function () {
                fillInAddress();
            });
        }

        autocomplete();
        autocomplete2();
    }
    function fillInAddress() {
        console.log('test2');
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var place2 = autocomplete2.getPlace();
        console.log(place);
        console.log(place2);
        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        for (var i = 0; i < place2.address_components.length; i++) {
            var addressType2 = place2.address_components[i].types[0];
            if (componentForm[addressType2]) {
                var val2 = place2.address_components[i][componentForm[addressType2]];
                document.getElementById(addressType2).value = val2;
            }
        }
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }

        console.log('test');

    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        console.log('test1');
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                var start = autocomplete.setBounds(circle.getBounds());
                var end = autocomplete2.setBounds(circle.getBounds());
                console.log(start);
                console.log(end);


            });
        }

    }


});