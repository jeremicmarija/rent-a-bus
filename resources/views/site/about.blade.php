@extends('site.layouts.app')

{{--@section('title')--}}
{{--Fudeks | {{trans('home.about_tab')}}--}}
{{--@stop--}}

@section('content')
    <div class="container">
        <h1 class="about-title text-uppercase">{{trans('about.title')}}</h1>
    </div>
    <div class="container">
        <div class="row abr">
            <div class="col-md-6">
                {{--<img class="img-responsive about-img first" src="{{asset('/assets/site/images/about/a1.png')}}" alt=""/>--}}
            </div>
            <div class="col-md-6">
                <p class="about-firstp">{{trans('about.about_1')}}</p>
            </div>
        </div>
        <div class="row abr">
            <div class="col-md-6 hidden_on_small">
                <p class="about-firstp nop">{{trans('about.about_2')}}</p>
                <p class="about-firstp nop">{{trans('about.about_3')}}</p>
            </div>
            <div class="col-md-6">
                {{--<img class="img-responsive about-img second" src="{{asset('/assets/site/images/about/a2.png')}}" alt=""/>--}}
            </div>
            <div class="col-md-6 display_on_small">
                <p class="about-firstp nop">{{trans('about.about_2')}}</p>
                <p class="about-firstp nop">{{trans('about.about_3')}}</p>
            </div>
        </div>
        <div class="row abr">
            <div class="col-md-6">
                {{--<img class="img-responsive about-img third" src="{{asset('/assets/site/images/about/a3.png')}}" alt=""/>--}}
            </div>
            <div class="col-md-6">
                <p class="about-firstp">{{trans('about.about_4')}}</p>
                <p class="about-firstp">{{trans('about.about_5')}}</p>
            </div>
        </div>
    </div>
    <div class="container"></div>
    <div class="gap"></div>
@stop