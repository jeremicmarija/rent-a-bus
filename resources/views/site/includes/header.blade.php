<header id="main-header">
    <div class="header-top">
        <div class="container">
            <div style="height: 40px;" class="row">
                <div class="col-xs-3 padding-reset">
                    {{--<a class="logo" href="{{route('home',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr'])}}">--}}
                        {{--<img src="{{asset('assets/site/images/logo.png')}}" alt="Fudeks rent a car" title="Image Title" />--}}
                    {{--</a>--}}
                </div>
                <div id="{{Auth::user() ? 'menu-container' : ''}}" class="{{Auth::user() ? '' : 'col-xs-9 pull-right'}}">
                    <div class="top-user-area clearfix">
                        <ul class="top-user-area-list list list-horizontal list-border {{Auth::user() ? 'ul-fix' : ''}}">
                            {{--@if(Auth::user())--}}
                                {{--<li class="top-user-area-avatar">--}}
                                    {{--<a href="{{ route('user.account',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.hi')}}, {{ Auth::user()->first_name}}</a>--}}
                                {{--</li>--}}
                                {{--<li><a href="{{ route('user.account',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.account')}}</a></li>--}}
                                {{--<li><a href="/logout">{{trans('home.logout')}}</a></li>--}}
                            {{--@else--}}
                                {{--<li><a href="{{ route('login',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.login')}}</a></li>--}}
                                {{--<li><a href="{{ route('register',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.register')}}</a></li>--}}
                            {{--@endif--}}
                            <li>
                                <div class="dropdown" id="lang_drop">
                                    <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        {{trans('home.lang')}}
                                        @if(App::getLocale() == 'en')
                                            <img class="flag" src="{{asset('assets/site/images/uk.png')}}" alt="English"/>
                                        @else
                                            <img class="flag" src="{{asset('assets/site/images/sr.png')}}" alt="Srpski"/>
                                        @endif
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li class="{{App::getLocale() == 'en' ? 'disabled' : ''}}">
                                            @if(\Illuminate\Support\Facades\Request::fullUrl() == \Illuminate\Support\Facades\Request::url())
                                                <a href="{{\Illuminate\Support\Facades\Request::fullUrl() . '?lang=en&trans=true'}}" id="en"><img class="flag" src="{{asset('assets/site/images/uk.png')}}" alt="English"/> EN</a>
                                            @else
                                                <a href="{{\Illuminate\Support\Facades\Request::fullUrl() . '&lang=en&trans=true&params=true'}}" id="en"><img class="flag" src="{{asset('assets/site/images/uk.png')}}" alt="English"/> EN</a>
                                            @endif
                                        </li>
                                        <li class="{{App::getLocale() == 'sr' ? 'disabled' : ''}}">
                                            @if(\Illuminate\Support\Facades\Request::fullUrl() == \Illuminate\Support\Facades\Request::url())
                                                <a href="{{\Illuminate\Support\Facades\Request::fullUrl() . '?lang=sr&trans=true'}}" id="sr"><img class="flag" src="{{asset('assets/site/images/sr.png')}}" alt="Serbian"/> SR</a>
                                            @else
                                                <a href="{{\Illuminate\Support\Facades\Request::fullUrl() . '&lang=sr&trans=true&params=true'}}" id="sr"><img class="flag" src="{{asset('assets/site/images/sr.png')}}" alt="Serbian"/> SR</a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="menu" class="container">
        <div class="nav">
            <ul class="slimmenu" id="slimmenu">
                <li class="{{ (\Illuminate\Support\Facades\Request::url() == route('home')) ? 'active' : ''}}"><a href="{{route('home',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr'])}}">{{trans('home.home')}}</a></li>
                <li class="{{ (\Illuminate\Support\Facades\Request::url() == route('quote')) ? 'active' : ''}}"><a href="{{ route('quote',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.quote')}}</a></li>
                <li class="{{ (\Illuminate\Support\Facades\Request::url() == route('buses')) ? 'active' : ''}}"><a href="{{ route('buses',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.buses')}}</a></li>

                {{--<li class="{{ (\Illuminate\Support\Facades\Request::url() == route('terms')) ? 'active' : ''}}"><a href="{{ route('terms',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.terms')}}</a></li>--}}
                <li class="{{ (\Illuminate\Support\Facades\Request::url() == route('about')) ? 'active' : ''}}"><a href="{{ route('about',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.about')}}</a></li>
                <li class="{{ (\Illuminate\Support\Facades\Request::url() == route('contact')) ? 'active' : ''}}"><a href="{{ route('contact',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">{{trans('home.contact')}}</a></li>
                @if(Auth::user())
                    @if(Auth::user()->role == 1)
                        <li><a href="{{ route('admin.home') }}">Admin</a></li>
                    @endif
                @endif
            </ul>
        </div>
    </div>

</header>