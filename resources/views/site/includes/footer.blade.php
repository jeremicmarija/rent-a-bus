<footer id="main-footer">
    <div class="container">
        <div class="row row-wrap">
            <div class="inner-row">
                <div class="col-md-3 text-center align_left">
                    {{--<h4 class="mb20">{{trans('home.follow')}}</h4>--}}
                    <ul class="list list-horizontal list-space social-ul">
                        <li>
                            <a class="fa fa-facebook box-icon-normal round animate-icon-bottom-to-top" target="_blank" href="https://www.facebook.com/FudeksRentACarBeograd/?ref=br_rs"></a>
                        </li>
                        <li>
                            <a class="fa fa-twitter box-icon-normal round animate-icon-bottom-to-top" target="_blank" href="https://twitter.com/fudeks_rentacar"></a>
                        </li>
                        <li>
                            <a class="fa fa-google-plus box-icon-normal round animate-icon-bottom-to-top" target="_blank" href="https://plus.google.com/104935735525211242297"></a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-5">
                    {{--<h4 class="text-center align_left news_title">{{trans('home.newsletter')}}</h4>--}}
                    {{--<form id="news" method="post" action="{{route('subscribe')}}">--}}
                        {{--{{csrf_field()}}--}}
                        {{--<div class="{{ $errors->has('submail') ? 'has-error' : '' }} align_left">--}}
                            {{--<div class="news-left">--}}
                                {{--<label class="text-center align_left">{{trans('home.stay_updated')}}</label>--}}
                                {{--<input type="text" class="form-control" id="submail" name="submail" placeholder="Email" required maxlength="100">--}}
                                {{--@if ($errors->has('submail'))--}}
                                    {{--<span class="help-block">--}}
                                    {{--<strong>*{{ $errors->first('submail')}}</strong>--}}
                                {{--</span>--}}
                                {{--@endif--}}
                                {{--@if (session('message'))--}}
                                    {{--<span class="text-success">--}}
                                    {{--<strong>*{{session('message')}}</strong>--}}
                                {{--</span>--}}
                                {{--@endif--}}
                                {{--<p class="mt5"><small>*{{trans('home.spam')}}</small></p>--}}
                            {{--</div>--}}
                            {{--<div class="news-right">--}}
                                {{--<input type="submit" class="btn btn-primary" value="{{trans('home.subscribe')}}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                </div>

                <div class="col-md-4 text-center align_left">
                    {{--<h4>{{trans('home.questions')}}</h4>--}}
                    {{--<h5 class="text-color">{{trans('home.phones')}}:</h5>--}}
                    <ul id="phones">
                        <li><h5 class="text-color">+381 (062) 8052-824</h5></li>
                        <li><h5 class="text-color">0800 000 007</h5></li>
                        <li><h5 class="text-color">+381 11 7620 255</h5></li>
                    </ul>
                    {{--<h5 class="text-color">{{trans('home.phone')}}/Viber/What's up: +381 (060) 729-5289</h5>--}}
                    <h5 class="text-color">fudeksrentabus@gmail.com</h5>
                    {{--<p>24/7 {{ trans('home.support') }}</p>--}}
                </div>
            </div>
            <div class="col-md-12 text-center row-wrap copyright">
                <small class="">COPYRIGHT 2016 - {{date('Y')}} &copy; FUDEKS RENT A BUS POWERED BY <a class="text-color" target="_blank" href="http://www.ownage.digital/">XSSYSTEMS</a></small>
            </div>
        </div>
    </div>
</footer>