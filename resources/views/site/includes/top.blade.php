<div class="top-area show-onload">
    <div class="bg-holder full">
        <div class="bg-mask"></div>
        <div class="bg-parallax"></div>
        <div class="bg-content">
            <div class="container">
                <div class="row">
                    <div class="search-holder">
                        <div class="search-tabs col-md-5 search-tabs-bg slideUp">
                            {{--<h1>{{trans('home.main_title')}}</h1>--}}
                            <div class="tabbable">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab-1">
                                        <form method="get" action="{{ route('quote.search',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">
                                            <h1>{{ trans('home.title') }}</h1>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                                        <label class="label-form">{{ trans('home.pick-loc') }}</label>
                                                        <div id="locationField">
                                                            <input name="pick_up_loc" class="search-auto form-control" id="autocomplete" value="" aria-required="true" placeholder="Enter your address"
                                                                   onFocus="geolocate()" type="text"></input>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                                        <label class="label-form">{{ trans('home.drop-loc') }}</label>
                                                        <div id="locationField">
                                                            <input name="drop_off_loc" class="search-auto form-control" id="autocomplete2" placeholder="Enter your address"
                                                                   onFocus="geolocate()" type="text"></input>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-daterange">
                                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                                            <label class="label-form">{{ trans('home.num-pass') }}</label>
                                                            <input type="number" min="1" max="100" name="num_pass" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                            <input type="hidden" name="lang" id="lang" value="{{\App::getLocale()}}">
                                            <input value="{{ trans('home.btn-text') }}" class="btn btn-primary btn-lg" type="submit">
                                            {{ csrf_field() }}
                                            </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 slider-text">
                        <h1>{{ trans('home.slide-h1') }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{--@section('scripts')--}}
    {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7dU1rftKPndLThKPGhAKPfvzg1dvCp8E&libraries=places&callback=initAutocomplete" async defer></script>--}}
    {{--<script src="{{ asset('assets/site/js/googlemaps.js') }}"></script>--}}
{{--@stop--}}







<script>


    var placeSearch, autocomplete, autocomplete2;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {

        var southWest = new google.maps.LatLng(34.5428, -31.4647999);
        var northEast = new google.maps.LatLng(82.1673907, 74.3555001);
        var myCustomEuropeBounds = new google.maps.LatLngBounds(southWest, northEast);
        var options = {
            bounds: myCustomEuropeBounds,
            strictBounds: true,
            types: ['(cities)']
        };

        function autocomplete() {
            new google.maps.places.Autocomplete(document.getElementById('autocomplete'), options);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                fillInAddress();
            });
        }

        function autocomplete2() {
            new google.maps.places.Autocomplete(document.getElementById('autocomplete2'), options);

            google.maps.event.addListener(autocomplete2, 'place_changed', function () {
                fillInAddress();
            });
        }

        autocomplete();
        autocomplete2();
    }
    function fillInAddress() {
        console.log('test2');
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var place2 = autocomplete2.getPlace();
        console.log(place);
        console.log(place2);
        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        for (var i = 0; i < place2.address_components.length; i++) {
            var addressType2 = place2.address_components[i].types[0];
            if (componentForm[addressType2]) {
                var val2 = place2.address_components[i][componentForm[addressType2]];
                document.getElementById(addressType2).value = val2;
            }
        }
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }

        console.log('test');

    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        console.log('test1');
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                var start = autocomplete.setBounds(circle.getBounds());
                var end = autocomplete2.setBounds(circle.getBounds());
                console.log(start);
                console.log(end);


            });
        }

    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7dU1rftKPndLThKPGhAKPfvzg1dvCp8E&libraries=places&callback=initAutocomplete"
        async defer></script>
