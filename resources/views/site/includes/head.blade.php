<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="audience" content="all" />

<meta name="author" content="Fudeks">

<meta name="copyright" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">



<!-- /GOOGLE FONTS -->
<link rel="stylesheet" href="{{ asset('assets/site/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/site/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('assets/site/css/icomoon.css') }}">
<link rel="stylesheet" href="{{ asset('assets/site/css/styles.css') }}">
<link rel="stylesheet" href="{{ asset('assets/site/css/main.css') }}">

{{--google maps api--}}
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">

