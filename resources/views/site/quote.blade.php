@extends('site.layouts.app')

@section('content')
    <div class="gap"></div>

    <div class="container">

        <div class="tabbable">
            <div class="col-md-12 nav-tabs-travel">
                <ul class="nav nav-pills nav-sm nav-no-br mb10" id="myTab">
                    <li id="road" class="active"><a href="#" data-toggle="tab">Round Trip</a>
                    </li>
                    <li id="oneway"><a href="#" data-toggle="tab">One Way</a>
                    </li>
                </ul>
            </div>

            <form id="reservation_form" method="post" action="{{ route('reservation',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">
                <div class="row">
                    <div class="search-holder col-md-6">
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                <label class="label-form">{{ trans('home.pick-loc') }}</label>
                                <div id="locationField">
                                    <input name="pick_up_loc" class="search-auto form-control" id="autocomplete" placeholder="Enter start location"
                                           onFocus="geolocate()" type="text" value="{{ $pick_up_loc }}"></input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                <label class="label-form">{{ trans('home.drop-loc') }}</label>
                                <div id="locationField">
                                    <input name="drop_off_loc" class="search-auto form-control" id="autocomplete2" placeholder=" Enter end location"
                                           onFocus="geolocate()" type="text" value="{{ $drop_off_loc }}"></input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('home.num-pass') }}</label>
                                <input type="number" name="num_pass" value="{{ $num_pass }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('home.ride-type') }}</label>
                                <select class=" form-control" name="category_bus" id="search_category">
                                    <option value="Standard">Standard</option>
                                    <option value="Luxory">Luxory</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('quote.journey-type') }}</label>
                                <select class=" form-control" name="travel_reason" id="search_category">
                                    <option value="Business travel">Business travel</option>
                                    <option value="Leisure travel">Leisure travel</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('quote.luggage') }}</label>
                                <input name="laguage" type="text" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="search-holder col-md-6">
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left">
                                <label class="label-form">{{ trans('quote.pick-date') }}</label>
                                <div class="col-md-6 ">
                                    <input id="datepicker" class="form-control" name="pick_up_date" type="text" data-date-format="dd/mm/yyyy" >
                                </div>
                                <div class="col-md-6 picker-date">
                                    <input name="pick_up_time" class="time-pick form-control" type="text" readonly="true"/>
                                </div>
                            </div>
                        </div>
                        <div id="drop_date_calendar" class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left">
                                <label class="label-form">{{ trans('quote.drop-date') }}</label>
                                <div class="col-md-6 ">
                                    <input id="datepicker1" class="form-control" name="drop_off_date" type="text" data-date-format="dd/mm/yyyy"/>
                                </div>
                                <div class="col-md-6 picker-date">
                                    <input name="drop_off_time" class="time-pick form-control" type="text" readonly="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('quote.company') }}</label>
                                <input name="company_name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('quote.contactfn') }}</label>
                                <input name="first_name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('quote.contactln') }}</label>
                                <input name="last_name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('quote.phone') }}</label>
                                <input name="phone" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('quote.mail') }}</label>
                                <input name="email" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="search-holder col-md-12">
                        <div class="col-md-12">
                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                <label class="label-form">{{ trans('quote.info') }}</label>
                                <textarea name="info" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <input type="hidden" name="lang" id="lang" value="{{\App::getLocale()}}">
                    <input value="{{ trans('home.btn-text') }}" class="btn btn-primary btn-lg" type="submit">
                    {{ csrf_field() }}
                    </div>

                </div>
            </form>

        </div>
    </div>



    <div class="gap gap-small"></div>

    <div class="gap"></div>

<script>
        var placeSearch, autocomplete, autocomplete2;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {

            var southWest = new google.maps.LatLng(34.5428, -31.4647999);
            var northEast = new google.maps.LatLng(82.1673907, 74.3555001);
            var myCustomEuropeBounds = new google.maps.LatLngBounds(southWest, northEast);
            var options = {
                bounds: myCustomEuropeBounds,
                strictBounds: true,
                types: ['(cities)']
            };

            function autocomplete() {
                new google.maps.places.Autocomplete(document.getElementById('autocomplete'), options);

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    fillInAddress();
                });
            }

            function autocomplete2() {
                new google.maps.places.Autocomplete(document.getElementById('autocomplete2'), options);

                google.maps.event.addListener(autocomplete2, 'place_changed', function () {
                    fillInAddress();
                });
            }

            autocomplete();
            autocomplete2();
        }
        function fillInAddress() {
            console.log('test2');
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            var place2 = autocomplete2.getPlace();
            console.log(place);
            console.log(place2);
            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            for (var i = 0; i < place2.address_components.length; i++) {
                var addressType2 = place2.address_components[i].types[0];
                if (componentForm[addressType2]) {
                    var val2 = place2.address_components[i][componentForm[addressType2]];
                    document.getElementById(addressType2).value = val2;
                }
            }
            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }

            console.log('test');

        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            console.log('test1');
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    var start = autocomplete.setBounds(circle.getBounds());
                    var end = autocomplete2.setBounds(circle.getBounds());
                    console.log(start);
                    console.log(end);


                });
            }

        }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7dU1rftKPndLThKPGhAKPfvzg1dvCp8E&libraries=places&callback=initAutocomplete"
        async defer></script>


@endsection
