@extends('site.layouts.app')

@section('title')
    Fudeks | Bus
@stop

@section('content')

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.html">Home</a>
            </li>
            <li><a href="#">United States</a>
            </li>
            <li><a href="#">New York (NY)</a>
            </li>
            <li><a href="#">New York City</a>
            </li>
            <li><a href="#">New York Activities</a>
            </li>
            <li class="active">Central Park Trip</li>
        </ul>
        <div class="booking-item-details">
            <header class="booking-item-header">
                <div class="row">
                    <div class="col-md-9">
                        <h2 class="lh1em">Central Park Trip</h2>
                        <p class="lh1em text-small"><i class="fa fa-map-marker"></i> 6782 Sarasea Circle, Siesta Key, FL 34242</p>
                        <ul class="list list-inline text-small">
                            <li><a href="#"><i class="fa fa-envelope"></i> Owner E-mail</a>
                            </li>
                            <li><a href="#"><i class="fa fa-home"></i> Owner Website</a>
                            </li>
                            <li><i class="fa fa-phone"></i> +1 (572) 426-3323</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <p class="booking-item-header-price"><small>price</small>  <span class="text-lg">Free</span>
                        </p>
                    </div>
                </div>
            </header>
            <div class="row">
                <div class="col-md-7">
                    <div class="tabbable booking-details-tabbable">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-camera"></i>Photos</a>
                            </li>
                            <li><a href="#google-map-tab" data-toggle="tab"><i class="fa fa-map-marker"></i>On the Map</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-1">
                                <div class="fotorama" data-allowfullscreen="true" data-nav="thumbs">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Upper Lake in New York Central Park" />
                                    <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" />
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Pictures at the museum" />
                                    {{--<img src="img/800x600.png" alt="Image Alternative text" title="Plunklock live in Cologne" />--}}
                                    {{--<img src="img/800x600.png" alt="Image Alternative text" title="AMaze" />--}}
                                    {{--<img src="img/800x600.png" alt="Image Alternative text" title="Old No7" />--}}
                                    {{--<img src="img/800x600.png" alt="Image Alternative text" title="The Big Showoff-Take 2" />--}}
                                    {{--<img src="img/800x600.png" alt="Image Alternative text" title="4 Strokes of Fun" />--}}
                                    {{--<img src="img/800x600.png" alt="Image Alternative text" title="Me with the Uke" />--}}
                                    {{--<img src="img/800x600.png" alt="Image Alternative text" title="Trebbiano Ristorante - japenese breakfast" />--}}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="google-map-tab">
                                <div id="map-canvas" style="width:100%; height:500px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="booking-item-meta">
                        <h2 class="lh1em mt40">Exeptional!</h2>
                        <h3>97% <small >of guests recommend</small></h3>
                        <div class="booking-item-rating">
                            <ul class="icon-list icon-group booking-item-rating-stars">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                            </ul><span class="booking-item-rating-number"><b >4.7</b> of 5 <small class="text-smaller">guest rating</small></span>
                            <p><a class="text-default" href="#">based on 1535 reviews</a>
                            </p>
                        </div>
                    </div>
                    <div class="gap gap-small">
                        <h3>Owner description</h3>
                        <p>Lacus odio porttitor sagittis netus nullam dictumst varius ultricies volutpat fringilla erat rutrum dis eleifend vestibulum a dui magna rutrum ut massa dictumst ultricies justo ante ullamcorper congue scelerisque fames risus nam ultrices enim himenaeos volutpat conubia habitasse ridiculus nullam erat nec nibh dictum pellentesque imperdiet ultrices tellus dictum quisque</p>
                    </div>
                    <a href="#" class="btn btn-primary btn-lg">Add to Trip</a>
                </div>
            </div>
        </div>
        <div class="gap"></div>
        <h3 class="mb20">Activity Reviews</h3>
        <div class="row row-wrap">
            <div class="col-md-8">
                <ul class="booking-item-reviews list">
                    <li>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="booking-item-review-person">
                                    <a class="booking-item-review-person-avatar round" href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Afro" />
                                    </a>
                                    <p class="booking-item-review-person-name"><a href="#">John Doe</a>
                                    </p>
                                    <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">142 Reviews</a></small>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="booking-item-review-content">
                                    <h5>"Nam condimentum purus dictumst"</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                    <p>Pellentesque bibendum vestibulum non sem penatibus morbi vulputate malesuada varius posuere dictum phasellus malesuada curae mattis sociis lectus conubia sociosqu<span class="booking-item-review-more"> Ultrices hac fermentum magnis interdum gravida vestibulum fringilla et volutpat sagittis placerat vitae in vulputate interdum magna cubilia faucibus sodales euismod fusce vel sit feugiat natoque orci posuere blandit ut nibh feugiat molestie adipiscing metus aenean convallis urna pellentesque penatibus suscipit enim potenti nascetur dapibus augue tristique neque amet a vel arcu</span>
                                    </p>
                                    <div class="booking-item-review-more-content">
                                        <p>Vel consectetur augue sodales suscipit adipiscing a et duis sed ut dictumst adipiscing at nisi varius proin tortor duis himenaeos felis tincidunt massa</p>
                                        <p>Semper elementum convallis placerat cras lorem eget lobortis pellentesque magna est augue eleifend enim sodales eleifend dui eget ante curabitur lacinia in maecenas mus egestas donec eleifend parturient leo parturient tortor dictum leo dictumst nascetur fusce</p>
                                        <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Sleep</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Location</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Service</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Clearness</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Rooms</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                    </div>
                                    <p class="booking-item-review-rate">Was this review helpful?
                                        <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 15</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="booking-item-review-person">
                                    <a class="booking-item-review-person-avatar round" href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Good job" />
                                    </a>
                                    <p class="booking-item-review-person-name"><a href="#">Minnie Aviles</a>
                                    </p>
                                    <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">106 Reviews</a></small>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="booking-item-review-content">
                                    <h5>"Arcu risus sociis suspendisse"</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                    <p>Nunc tristique sapien vel ac metus amet gravida mi congue per elit aliquet nunc etiam id tempus nam vehicula vestibulum tristique suspendisse eu pretium libero a himenaeos tellus hac velit sem consequat vivamus dignissim<span class="booking-item-review-more"> Porta porttitor cum per nostra feugiat duis taciti per dui adipiscing quam lectus bibendum ullamcorper rutrum aliquet curabitur proin vehicula amet cubilia sit porta mus et aliquam accumsan cursus eros porta habitasse vitae feugiat per platea nascetur dictumst eu magnis aliquet aptent imperdiet pulvinar viverra</span>
                                    </p>
                                    <div class="booking-item-review-more-content">
                                        <p>Pellentesque feugiat tincidunt malesuada dui varius turpis dictum lectus neque</p>
                                        <p>Est consectetur mus bibendum varius tortor etiam sapien in nulla senectus nam netus tincidunt ullamcorper pretium ac rutrum ridiculus conubia nisl tristique congue congue suspendisse fusce curabitur metus pulvinar platea non posuere potenti amet</p>
                                        <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Sleep</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Location</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Service</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Clearness</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Rooms</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                    </div>
                                    <p class="booking-item-review-rate">Was this review helpful?
                                        <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 7</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="booking-item-review-person">
                                    <a class="booking-item-review-person-avatar round" href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Me with the Uke" />
                                    </a>
                                    <p class="booking-item-review-person-name"><a href="#">Cyndy Naquin</a>
                                    </p>
                                    <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">25 Reviews</a></small>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="booking-item-review-content">
                                    <h5>"Posuere massa pulvinar a"</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                    <p>Cursus est fames ornare dis sit hac netus metus mollis imperdiet tristique hendrerit eleifend habitant dignissim lectus est potenti mus venenatis luctus libero nec pellentesque dui<span class="booking-item-review-more"> Aliquam habitasse pulvinar faucibus justo luctus urna consectetur class ante leo lorem ac pellentesque ac primis orci inceptos sociis pretium venenatis suspendisse orci sed laoreet cubilia ornare sem aliquam sociosqu nec blandit egestas aliquam ornare maecenas vitae fringilla natoque fusce quis sit natoque magna molestie condimentum hendrerit facilisi risus est diam netus ligula tristique praesent potenti</span>
                                    </p>
                                    <div class="booking-item-review-more-content">
                                        <p>Aliquet condimentum dui mauris magnis lorem iaculis gravida facilisi class lacinia aliquam integer nascetur bibendum urna facilisi vivamus natoque tincidunt urna urna nisi eleifend</p>
                                        <p>Rutrum mattis in senectus senectus fermentum per vulputate mattis leo ornare nisi sociis maecenas amet vestibulum elementum mi bibendum velit posuere dapibus lorem tristique posuere massa lectus parturient dolor conubia posuere morbi penatibus</p>
                                        <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Sleep</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Location</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Service</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Clearness</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Rooms</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                    </div>
                                    <p class="booking-item-review-rate">Was this review helpful?
                                        <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 17</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="booking-item-review-person">
                                    <a class="booking-item-review-person-avatar round" href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Chiara" />
                                    </a>
                                    <p class="booking-item-review-person-name"><a href="#">Carol Blevins</a>
                                    </p>
                                    <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">111 Reviews</a></small>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="booking-item-review-content">
                                    <h5>"Convallis maecenas laoreet arcu"</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                    <p>Congue magnis parturient taciti curae velit aptent aliquet tellus nibh vitae arcu fringilla non erat tincidunt cum pulvinar enim vulputate interdum dignissim scelerisque habitant porttitor platea elementum justo conubia libero<span class="booking-item-review-more"> Lacinia dictum etiam dolor platea class elit cum tincidunt conubia nunc accumsan habitant quam mauris ad urna varius malesuada tempor vitae curae phasellus semper felis himenaeos ornare morbi fames aptent magnis facilisis rutrum dapibus senectus risus commodo dignissim potenti donec class blandit quisque fames condimentum fermentum auctor facilisi viverra tellus eleifend</span>
                                    </p>
                                    <div class="booking-item-review-more-content">
                                        <p>Torquent auctor dis fringilla felis donec est vitae eu purus nam elit ad metus aliquam pulvinar eget tempus</p>
                                        <p>Dapibus morbi cubilia per ultrices et luctus eget rhoncus ac libero interdum pharetra fusce rutrum mollis congue hac platea luctus aliquet per torquent nam varius hendrerit non odio risus aenean mus amet ad quisque velit nullam nunc curae metus proin blandit felis maecenas donec habitant semper</p>
                                        <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Sleep</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Location</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Service</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Clearness</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Rooms</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                    </div>
                                    <p class="booking-item-review-rate">Was this review helpful?
                                        <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 6</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="booking-item-review-person">
                                    <a class="booking-item-review-person-avatar round" href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Gamer Chick" />
                                    </a>
                                    <p class="booking-item-review-person-name"><a href="#">Cheryl Gustin</a>
                                    </p>
                                    <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">146 Reviews</a></small>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="booking-item-review-content">
                                    <h5>"Morbi imperdiet tortor quisque varius consequat"</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                    <p>Platea purus iaculis quam dis suspendisse augue consequat in porta quis nec purus habitasse orci velit dui justo leo nostra etiam<span class="booking-item-review-more"> Vulputate imperdiet id tristique per dictumst non nam mollis arcu cubilia dui dignissim proin felis fringilla primis justo viverra tincidunt pulvinar sociis maecenas gravida felis dolor penatibus pharetra aptent elementum taciti habitant aptent dapibus urna fringilla tortor porttitor donec commodo platea arcu molestie non ad vestibulum erat</span>
                                    </p>
                                    <div class="booking-item-review-more-content">
                                        <p>Fusce vitae odio egestas enim curabitur sagittis ridiculus senectus parturient orci purus accumsan at ullamcorper magnis facilisi maecenas senectus suscipit odio massa class dictum mattis duis ut facilisis tempus dui augue natoque duis nibh primis sociis magna vehicula nostra lacus dapibus himenaeos vel dolor duis</p>
                                        <p>Libero ligula magnis consectetur facilisi habitasse curae semper maecenas habitasse metus scelerisque consectetur placerat leo aenean sagittis arcu eros vitae netus porta senectus non consectetur felis litora inceptos ligula cras aliquam fames dui tempus tempor sed luctus quis sociis tellus sociosqu magnis elit venenatis in faucibus</p>
                                        <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Sleep</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Location</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Service</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Clearness</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Rooms</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                    </div>
                                    <p class="booking-item-review-rate">Was this review helpful?
                                        <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 10</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="booking-item-review-person">
                                    <a class="booking-item-review-person-avatar round" href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="AMaze" />
                                    </a>
                                    <p class="booking-item-review-person-name"><a href="#">Joe Smith</a>
                                    </p>
                                    <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">106 Reviews</a></small>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="booking-item-review-content">
                                    <h5>"Venenatis ante proin"</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                    <p>Ultrices feugiat non velit mollis dictumst nisi pellentesque torquent tellus magnis ultricies dis elementum est habitant diam nullam iaculis aliquam aliquet tempus scelerisque nascetur mattis gravida<span class="booking-item-review-more"> A amet sem suspendisse vehicula arcu parturient rhoncus consectetur porttitor erat ultrices lobortis natoque gravida pellentesque lorem quisque cras senectus commodo suspendisse gravida quis senectus vestibulum dis fames nisl pharetra inceptos semper ullamcorper auctor dictum conubia praesent quis eros neque magnis senectus sollicitudin aliquam donec taciti pharetra semper fermentum vitae vehicula risus sem purus in erat cubilia et egestas condimentum etiam curabitur cursus</span>
                                    </p>
                                    <div class="booking-item-review-more-content">
                                        <p>Pellentesque porttitor lorem lacus rhoncus himenaeos sociis pulvinar ridiculus quam fermentum felis metus senectus molestie tempor habitant ante nascetur nunc congue inceptos ac faucibus cras fringilla velit convallis etiam sociis ac sed tempus hendrerit enim mus sagittis torquent etiam commodo gravida tincidunt</p>
                                        <p>Dignissim pellentesque id felis nec vel eros ornare mi dis sem suscipit orci congue imperdiet integer ad turpis sociis eros eleifend fermentum luctus tristique inceptos mus dictum euismod condimentum fringilla eu torquent laoreet tempor nulla</p>
                                        <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Sleep</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Location</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Service</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Clearness</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Rooms</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                    </div>
                                    <p class="booking-item-review-rate">Was this review helpful?
                                        <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 8</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="booking-item-review-person">
                                    <a class="booking-item-review-person-avatar round" href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Bubbles" />
                                    </a>
                                    <p class="booking-item-review-person-name"><a href="#">Ava McDonald</a>
                                    </p>
                                    <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">51 Reviews</a></small>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="booking-item-review-content">
                                    <h5>"Phasellus volutpat tristique montes conubia"</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                    <p>Taciti nunc torquent dignissim parturient risus ad habitant leo nascetur quis nascetur sociosqu posuere libero felis ultricies lacus vel nam facilisis lectus nisi ridiculus aptent vestibulum eleifend ultrices rutrum primis dolor lorem neque varius habitant inceptos<span class="booking-item-review-more"> Aliquet feugiat ad dapibus erat class aptent purus senectus vitae scelerisque nisl sit diam class posuere tempor lacinia euismod mi ante varius duis aliquet vel laoreet eleifend porttitor per convallis dictum maecenas scelerisque purus inceptos suspendisse laoreet malesuada primis scelerisque diam consectetur mauris rhoncus condimentum</span>
                                    </p>
                                    <div class="booking-item-review-more-content">
                                        <p>Scelerisque nostra primis senectus eu habitasse cursus justo rutrum neque aenean magna accumsan ornare scelerisque convallis dignissim imperdiet leo ut amet curabitur lacus vulputate rutrum risus primis</p>
                                        <p>Tristique conubia nostra sed quis facilisis massa nullam cubilia ad sapien per cursus malesuada fermentum tortor interdum litora lacus risus vitae conubia vestibulum etiam leo dui etiam tempor urna ut dolor molestie ac ad blandit praesent rutrum pharetra ridiculus integer sodales nullam non ante eu class</p>
                                        <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Sleep</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Location</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Service</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="list booking-item-raiting-summary-list">
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Clearness</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-raiting-list-title">Rooms</div>
                                                        <ul class="icon-group booking-item-rating-stars">
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                            <li><i class="fa fa-smile-o"></i>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                    </div>
                                    <p class="booking-item-review-rate">Was this review helpful?
                                        <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 15</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="row wrap">
                    <div class="col-md-5">
                        <p><small>1345 reviews on this activity. &nbsp;&nbsp;Showing 1 to 7</small>
                        </p>
                    </div>
                    <div class="col-md-7">
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a>
                            </li>
                            <li><a href="#">2</a>
                            </li>
                            <li><a href="#">3</a>
                            </li>
                            <li><a href="#">4</a>
                            </li>
                            <li><a href="#">5</a>
                            </li>
                            <li><a href="#">6</a>
                            </li>
                            <li><a href="#">7</a>
                            </li>
                            <li class="dots">...</li>
                            <li><a href="#">43</a>
                            </li>
                            <li class="next"><a href="#">Next Page</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="gap gap-small"></div>
                <div class="box bg-gray">
                    <h3>Write a Review</h3>
                    <form>
                        <div class="form-group">
                            <label>Review Title</label>
                            <input class="form-control" type="text" />
                        </div>
                        <div class="form-group">
                            <label>Review Text</label>
                            <textarea class="form-control" rows="6"></textarea>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Leave a Review" />
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <h4>Activities Near</h4>
                <ul class="booking-list">
                    <li>
                        <div class="booking-item booking-item-small">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Street Yoga" />
                                </div>
                                <div class="col-xs-5">
                                    <h5 class="booking-item-title">Street Yoga</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star-half-empty"></i>
                                        </li>
                                        <li><i class="fa fa-star-o"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-3"><span class="booking-item-price">$115</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="booking-item booking-item-small">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" />
                                </div>
                                <div class="col-xs-5">
                                    <h5 class="booking-item-title">Manhattan Skyline</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star-half-empty"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-3"><span class="booking-item-price">Free</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="booking-item booking-item-small">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                </div>
                                <div class="col-xs-5">
                                    <h5 class="booking-item-title">Adrenaline Madness</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star-half-empty"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-3"><span class="booking-item-price">$105</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="booking-item booking-item-small">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Pictures at the museum" />
                                </div>
                                <div class="col-xs-5">
                                    <h5 class="booking-item-title">The Metropolitan Museum of Art</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star-half-empty"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-3"><span class="booking-item-price">$35</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="booking-item booking-item-small">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Upper Lake in New York Central Park" />
                                </div>
                                <div class="col-xs-5">
                                    <h5 class="booking-item-title">Central Park Trip</h5>
                                    <ul class="icon-group booking-item-rating-stars">
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star"></i>
                                        </li>
                                        <li><i class="fa fa-star-half-empty"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-3"><span class="booking-item-price">Free</span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="gap gap-small"></div>
    </div>




@endsection