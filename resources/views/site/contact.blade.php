@extends('site.layouts.app')

@section('title')
    Fudeks | {{trans('contact.contact_tab')}}
@stop

@section('content')
    <div class="container">
        <h1 class="page-title">{{trans('contact.contact')}}</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>{{trans('contact.mess')}}</p>
                {{--<form class="mt30 cont-form" method="post" action="{{route('sendMail', ['lang'=>App::getLocale() == 'en' ? 'en' : 'sr'])}}">--}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('contactName') ? ' has-error' : '' }}">
                                <label for="contactName">{{trans('contact.name')}}</label>
                                <input class="form-control" type="text" id="contactName" name="contactName" value="{{old('contactName')}}"/>
                                @if ($errors->has('contactName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contactName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('contactEmail') ? ' has-error' : '' }}">
                                <label for="contactEmail">E-mail</label>
                                <input class="form-control" type="text" name="contactEmail"  value="{{old('contactEmail')}}"/>
                                @if ($errors->has('contactEmail'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contactEmail') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('contactBody') ? ' has-error' : '' }}">
                        <label for="contactBody">{{trans('contact.mess1')}}</label>
                        <textarea class="form-control" name="contactBody"  placeholder="{{trans('contact.text_mess')}}..">{{old('contactBody')}}</textarea>
                        @if ($errors->has('contactBody'))
                            <span class="help-block">
                                <strong>{{ $errors->first('contactBody') }}</strong>
                            </span>
                        @endif
                    </div>
                    @if(Session::has('message'))
                        <div class="alert alert-info text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{Session::get('message')}}
                        </div>
                    @endif
                    @if(Session::has('fail'))
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{Session::get('fail')}}
                        </div>
                    @endif
                    <input class="btn btn-primary" type="submit" value="{{trans('contact.send')}}" />
                {{csrf_field()}} <!-- Security hidden input field -->
                {{--</form>--}}
            </div>
            <div class="col-md-6">
                <aside class="sidebar-right">
                    <ul class="address-list list">
                        <li>
                            <h5><i class="fa fa-envelope g-grey" aria-hidden="true"></i> Email</h5><a target="_blank" href="mailto:srdjan.jagodic@gmail.com">srdjan.jagodic@gmail.com</a>
                        </li>
                        <li>
                            <h5><i class="fa fa-phone g-theme" aria-hidden="true"></i> {{trans('home.phones')}}</h5><p>+381 (062) 805-2824</p><p>0800 000 007</p><p>+381 11 7620 255</p>
                        </li>
                        <li>
                            <h5><i class="fa fa-phone g-theme" aria-hidden="true"></i> {{trans('home.phone')}}/Viber/What's up</h5><p>+381 (060) 729-5289</p>
                        </li>
                        <li>
                            <h5><i class="fa fa-map-marker g-red" aria-hidden="true"></i> {{trans('contact.address')}}</h5><address><br/>Balkanska 47, {{trans('contact.bg')}}<br/>Tabanovacka 21 ({{trans('contact.add')}}), {{trans('contact.bg')}}<br/>{{trans('contact.serbia')}}<br /></address>
                        </li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
    <div class="gap-small"></div>
    <div class="container-fluid padding-reset" style="margin-bottom: -10px;">
        <div class="row padding-reset">
            <div class="col-md-12 padding-reset">
                <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1415.3156319391464!2d20.458250466738495!3d44.80870285629608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a7aa96ca185d7%3A0xb7a799cf76f1db3d!2sFudeks!5e0!3m2!1sen!2srs!4v1482486622025" width="100%" height="350px" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
@stop