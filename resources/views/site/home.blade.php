@extends('site.layouts.app')

@section('title')
    Fudeks | Rent a bus
@stop

@section('content')
    <!-- TOP AREA -->
    @include('site.includes.top')
    <!-- END TOP AREA  -->
    {{--About--}}
    <div id="home_about" class="container-fluid">
        <div class="container text-center">
            <h1 class="booking-title">{{trans('home.about')}}</h1>

            <p>{{trans('home.h_about')}}</p>
        </div>
    </div>
    {{--<div class="container">--}}
        {{--<div class="gap gap-small"></div>--}}
        {{--<h1 class="text-center booking-title">{{trans('home.top')}}</h1>--}}

        {{--<div class="row row-wrap">--}}
            {{--@foreach($cars as $car)--}}
                {{--<div class="col-xs-12 col-sm-6 col-md-3">--}}
                    {{--<div>--}}
                        {{--@if($car->pictures != '[]')--}}
                            {{--<?php--}}
                            {{--$pictures = json_decode($car->pictures, true);--}}
                            {{--?>--}}
                        {{--@endif--}}
                        {{--<header class="thumb-header">--}}
                            {{--<a href="{{route('car.details',['params'=>implode('-',explode(' ',strtolower($car->brand))) . '-' . implode('-',explode('.',implode('-',explode(' ',strtolower($car->model))))),'lang'=>App::getLocale() == 'en' ? 'en' : 'sr','id'=>$car->id])}}">--}}
                                {{--<div class="home-img-container">--}}
                                    {{--<img class="img-responsive"--}}
                                         {{--src="{{ asset('assets/site/images/cars/' . $pictures[0]) }}"--}}
                                         {{--alt="{{$car->brand . $car->model . ' - Fudeks rent a car'}}"--}}
                                         {{--title="{{$car->brand . " " . $car->model}}"/>--}}
                                {{--</div>--}}
                            {{--</a>--}}

                            {{--<div class="clear"></div>--}}
                        {{--</header>--}}
                        {{--<div class="thumb-caption">--}}
                            {{--<h5 class="thumb-title"><a class="text-darken"--}}
                                                       {{--href="/cars/car/{{$car->id}}-{{implode('-',explode(' ',$car->brand))}}-{{implode('-',explode(' ',$car->model))}}">{{ $car->brand }} {{ $car->model }}</a>--}}
                                {{--@if(Auth::user())--}}
                                    {{--@if(Auth::user()->role == 1)--}}
                                        {{--<span class="pull-right"><a class="btn btn-info btn-sm" title="Edit"--}}
                                                                    {{--href="{{ route('admin.cars.edit',['id'=>$car->id]) }}">{{trans('home.edit')}}</a></span>--}}
                                    {{--@endif--}}
                                {{--@endif--}}
                                {{--@foreach($categories as $cat)--}}
                                    {{--@if($cat->id == $car->category->id)--}}
                                        {{--<?php--}}
                                        {{--if ( \App::getLocale() == 'sr' )--}}
                                        {{--{--}}
                                            {{--$name = $cat->translation ? $cat->translation->sr : $cat->name;--}}
                                        {{--} else--}}
                                        {{--{--}}
                                            {{--$name = $cat->name;--}}
                                        {{--}--}}
                                        {{--?>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</h5>--}}
                            {{--<small>{{trans('home.car_cat')}}: {{ $name }}</small>--}}
                            {{--<ul class="booking-item-features booking-item-features-small booking-item-features-sign clearfix mt5 home-icons">--}}
                                {{--<li rel="tooltip" data-placement="top" title="{{trans('cars.pass')}}"><i--}}
                                            {{--class="fa fa-male"></i><span--}}
                                            {{--class="booking-item-feature-sign">x {{ $car->passengers }}</span></li>--}}
                                {{--<li rel="tooltip" data-placement="top" title="{{trans('cars.doors')}}"><i--}}
                                            {{--class="im im-car-doors"></i><span--}}
                                            {{--class="booking-item-feature-sign">x {{ $car->doors }}</span></li>--}}
                                {{--<li rel="tooltip" data-placement="top" title="{{trans('cars.bag_qty')}}"><i--}}
                                            {{--class="fa fa-briefcase"></i><span class="booking-item-feature-sign">{{ $car->baggage }}--}}
                                        {{--l</span></li>--}}
                                {{--<?php--}}
                                {{--if ( \App::getLocale() == 'sr' )--}}
                                {{--{--}}
                                    {{--$transmiss = $car->translation('transmission') ? $car->translation('transmission')->sr : $car->transmission;--}}
                                    {{--$fuel = $car->translation('fuel') ? $car->translation('fuel')->sr : $car->fuel;--}}
                                {{--} else--}}
                                {{--{--}}
                                    {{--$transmiss = $car->transmission;--}}
                                    {{--$fuel = $car->fuel;--}}
                                {{--}--}}
                                {{--$transmission = explode('-', $transmiss);--}}
                                {{--?>--}}
                                {{--<li rel="tooltip" data-placement="top" title="{{trans('cars.trans')}}"><i--}}
                                            {{--class="im im-shift-auto"></i><span--}}
                                            {{--class="booking-item-feature-sign">{{ $transmission[0] }}</span></li>--}}
                                {{--<li rel="tooltip" data-placement="top" title="{{trans('cars.fuel')}}"><i--}}
                                            {{--class="im im-diesel"></i><span--}}
                                            {{--class="booking-item-feature-sign">{{ $fuel}}</span></li>--}}
                            {{--</ul>--}}
                            {{--<ul class="booking-item-features booking-item-features-small clearfix home-icons">--}}
                                {{--@foreach($car->details as $detail)--}}
                                    {{--<?php--}}
                                    {{--if ( \App::getLocale() == 'sr' )--}}
                                    {{--{--}}
                                        {{--$name = $detail->translation ? $detail->translation->sr : $detail->name;--}}
                                    {{--} else--}}
                                    {{--{--}}
                                        {{--$name = $detail->name;--}}
                                    {{--}--}}
                                    {{--?>--}}

                                    {{--<li rel="tooltip" data-placement="top" title="{{ $name }}"><i--}}
                                                {{--class="{{$detail->icon}}"></i></li>--}}

                                {{--@endforeach--}}
                            {{--</ul>--}}
                            {{--@if($car->prices->where('period_start','30')[7]->action_price != null)--}}
                                {{--<p class="text-darken mb0 text-color ib">{{trans('home.price_text')}} {{ $car->prices->where('period_start','30')[7]->action_price }} &euro;<small>--}}
                                        {{--/{{trans('home.day')}}  </small>--}}
                                {{--</p>--}}
                                {{--<span class="wrap ib">--}}
                                {{--@if($is_summer_time)--}}
                                        {{--<p class="text-darken mb0 text-color"><span class="line-trough">({{ $car->prices->where('period_start','30')[7]->summer_price != null ? $car->prices->where('period_start','30')[7]->summer_price : $car->prices->where('period_start','30')[7]->price }} &euro;</span>--}}
                                            {{--<small> /{{trans('home.day')}})</small>--}}
                                        {{--</p>--}}
                                    {{--@else--}}
                                        {{--<p class="text-darken mb0 text-color"><span class="line-trough">({{ $car->prices->where('period_start','30')[7]->price }} &euro;</span>--}}
                                            {{--<small> /{{trans('home.day')}})</small>--}}
                                        {{--</p>--}}
                                    {{--@endif--}}
                                {{--</span>--}}
                                {{--<p class="text-darken mb0">--}}
                                    {{--<small>{{trans('home.deposit')}}: {{round($car->deposit)}} &euro;</small>--}}
                                {{--</p>--}}
                            {{--@else--}}
                                {{--@if($is_summer_time)--}}
                                    {{--<p class="text-darken mb0 text-color">{{trans('home.price_text')}} {{ $car->prices->where('period_start','30')[7]->summer_price != null ? $car->prices->where('period_start','30')[7]->summer_price : $car->prices->where('period_start','30')[7]->price }} &euro;<small>--}}
                                            {{--/{{trans('home.day')}}</small>--}}
                                    {{--</p>--}}
                                {{--@else--}}
                                    {{--<p class="text-darken mb0 text-color">{{trans('home.price_text')}} {{ $car->prices->where('period_start','30')[7]->price }} &euro;<small>--}}
                                            {{--/{{trans('home.day')}}</small>--}}
                                    {{--</p>--}}
                                {{--@endif--}}
                                {{--<p class="text-darken mb0">--}}
                                    {{--<small>{{trans('home.deposit')}}: {{round($car->deposit)}} &euro;</small>--}}
                                {{--</p>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endforeach--}}
        {{--</div>--}}
    {{--</div>--}}

    <hr class="separator">
    {{--Carousel--}}
    {{--<div class="container">--}}
        {{--<div class="col-xs-12">--}}
            {{--<div class="home-carousel">--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/chervolet.png')}}"--}}
                                                          {{--class="img-responsive" alt="Chevrolet"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/dacia.png')}}"--}}
                                                          {{--class="img-responsive" alt="Dacia"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/fiat.png')}}"--}}
                                                          {{--class="img-responsive" alt="Fiat"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/ford-2.png')}}"--}}
                                                          {{--class="img-responsive" alt="Ford"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/opel.png')}}"--}}
                                                          {{--class="img-responsive" alt="Opel"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/pegouete.png')}}"--}}
                                                          {{--class="img-responsive" alt="Peugeot"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/vw.png')}}"--}}
                                                          {{--class="img-responsive" alt="Volkswagen"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/seat.png')}}"--}}
                                                          {{--class="img-responsive" alt="Volkswagen"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/kia.png')}}"--}}
                                                          {{--class="img-responsive" alt="Volkswagen"></div>--}}
                {{--<div class="carousel-logo-container"><img src="{{asset('assets/site/images/logos/hyundai.png')}}"--}}
                                                          {{--class="img-responsive" alt="Volkswagen"></div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--End carousel--}}

    <div class="cta-bg">
        <div class="cta-mask">
            <div class="container">
                <div class="row">
                    <div class="cta-holder">
                        <div class="col-md-8">
                            {{--<h2 class="cta">{{trans('home.cta')}}</h2>--}}
                        </div>
                        <div class="col-md-4">
                            {{--<a href="#main-header" class="cta-button">{{trans('home.syc')}}</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@stop