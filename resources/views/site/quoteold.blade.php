@extends('site.layouts.app')

@section('content')
    <div class="gap"></div>

<div class="container">

        <div class="tabbable">
            <div class="col-md-12 nav-tabs-travel">
                <ul class="nav nav-pills nav-sm nav-no-br mb10" id="myTab">
                    <li id="road" class="active"><a href="#flight-search-1" data-toggle="tab">Round Trip</a>
                    </li>
                    <li id="oneway"><a href="#flight-search-2" data-toggle="tab">One Way</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane fade in active" id="flight-search-1">

                    <form method="post" action="{{ route('reservation',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr']) }}">
                        <div class="col-md-12">
                            <input type="hidden" name="lang" id="lang" value="{{\App::getLocale()}}">
                            <input value="{{ trans('home.btn-text') }}" class="btn btn-primary btn-lg" type="submit">
                            {{ csrf_field() }}
                        </div>
                        <div class="row">
                            <div class="search-holder col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                        <label class="label-form">{{ trans('home.pick-loc') }}</label>
                                        <div id="locationField">
                                            <input name="pick_up_loc" class="search-auto form-control" id="autocomplete" placeholder="eNTER YOUR ADRESS"
                                                   onFocus="geolocate()" type="text" value="{{ $pick_up_loc }}"></input>
                                            {{--<input id="pickup_lon" name="pickup_lon" type="hidden" value="" />--}}
                                            {{--<input id="pickup_lat" name="pickup_lat" type="hidden" value="" />--}}
                                            <input id="pick_up_name_old" name="pick_up_name_old" type="hidden" value="{{  $pick_up_loc  }}" />
                                            {{--<input id="pick_up_name_new" name="pick_up_name_new" type="hidden" value="{{  $pick_up_loc_new ?    $pick_up_loc_new : ''}}" />--}}

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                        <label class="label-form">{{ trans('home.drop-loc') }}</label>
                                        <div id="locationField">
                                            <input name="drop_off_loc" class="search-auto form-control" id="autocomplete2" placeholder="{{ $drop_off_loc }}"
                                                   onFocus="geolocate()" type="text" value="{{ $drop_off_loc }}"></input>
                                            {{--<input id="dropoff_lon" name="dropoff_lon" type="hidden" value="" />--}}
                                            {{--<input id="dropoff_lat" name="dropoff_lat" type="hidden" value="" />--}}
                                            <input id="drop_off_name_old" name="drop_off_name_old" type="hidden" value="{{ $drop_off_loc }}" />
                                            {{--<input id="drop_off_name_new" name="drop_off_name_new" type="hidden" value="{{ $drop_off_locc_new ?    $drop_off_loc_new : '' }}" />--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('home.num-pass') }}</label>
                                        <input type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('home.ride-type') }}</label>
                                        <select class=" form-control" name="search_category" id="search_category">
                                            <option value="1">Standard</option>
                                            <option value="2">Luxory</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('quote.journey-type') }}</label>
                                        <select class=" form-control" name="search_category" id="search_category">
                                            <option value="1">Business travel</option>
                                            <option value="2">Leisure travel</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('quote.luggage') }}</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="search-holder col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                        <label class="label-form">{{ trans('quote.pick-date') }}</label>
                                        <div class="col-md-6 picker-date">
                                            <input id="pick-up-date" class="date-pick form-control" name="pick_up_date" type="text" readonly="true" data-date-format="dd/mm/yyyy"/>
                                        </div>
                                        <div class="col-md-6 picker-date">
                                            <input name="pick_up_time" class="time-pick form-control" type="text" readonly="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                        <label class="label-form">{{ trans('quote.drop-date') }}</label>
                                        <div class="col-md-6 picker-date">
                                            <input id="pick-up-date" class="date-pick form-control" name="drop_off_date" type="text" readonly="true" data-date-format="dd/mm/yyyy"/>
                                        </div>
                                        <div class="col-md-6 picker-date">
                                            <input name="pick_up_time" class="time-pick form-control" type="text" readonly="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('quote.company') }}</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('quote.contactfn') }}</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('quote.contactln') }}</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('quote.phone') }}</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('quote.mail') }}</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="search-holder col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>
                                        <label class="label-form">{{ trans('quote.info') }}</label>
                                        <textarea rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-md-12">--}}
                                {{--<input type="hidden" name="lang" id="lang" value="{{\App::getLocale()}}">--}}
                                {{--<input value="{{ trans('home.btn-text') }}" class="btn btn-primary btn-lg" type="submit">--}}
                                {{--{{ csrf_field() }}--}}
                            {{--</div>--}}

                        </div>
                    </form>

                </div>
            </div>
        </div>
                {{--<div class="tab-pane fade" id="flight-search-2">--}}
                    {{--<form>--}}
                        {{--<div class="row">--}}
                            {{--<div class="search-holder col-md-6">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>--}}
                                        {{--<label class="label-form">{{ trans('home.pick-loc') }}</label>--}}
                                        {{--<select class="form-control" name="pick_up_loc" id="pick_up_loc">--}}
                                            {{--<option value="1">Beograd</option>--}}
                                            {{--<option value="2">Novi Sad</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>--}}
                                        {{--<label class="label-form">{{ trans('home.drop-loc') }}</label>--}}
                                        {{--<select class="form-control" name="drop_off_loc" id="drop_off_loc">--}}
                                            {{--<option value="1">Beograd</option>--}}
                                            {{--<option value="2">Novi Sad</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('home.num-pass') }}</label>--}}
                                        {{--<input type="number" class="form-control">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('home.ride-type') }}</label>--}}
                                        {{--<select class=" form-control" name="search_category" id="search_category">--}}
                                            {{--<option value="1">Standard</option>--}}
                                            {{--<option value="2">Luxory</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.journey-type') }}</label>--}}
                                        {{--<select class=" form-control" name="search_category" id="search_category">--}}
                                            {{--<option value="1">Business travel</option>--}}
                                            {{--<option value="2">Leisure travel</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.luggage') }}</label>--}}
                                        {{--<input type="text" class="form-control">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="search-holder col-md-6">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.pick-date') }}</label>--}}
                                        {{--<div class="col-md-6 picker-date">--}}
                                            {{--<input id="pick-up-date" class="date-pick form-control" name="pick_up_date" type="text" readonly="true" data-date-format="dd/mm/yyyy"/>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6 picker-date">--}}
                                            {{--<input name="pick_up_time" class="time-pick form-control" type="text" readonly="true"/>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                               {{----}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.company') }}</label>--}}
                                        {{--<input type="text" class="form-control">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.contactfn') }}</label>--}}
                                        {{--<input type="text" class="form-control">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.contactln') }}</label>--}}
                                        {{--<input type="text" class="form-control">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.phone') }}</label>--}}
                                        {{--<input type="text" class="form-control">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.mail') }}</label>--}}
                                        {{--<input type="text" class="form-control">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="search-holder col-md-12">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-list-alt input-icon" aria-hidden="true"></i>--}}
                                        {{--<label class="label-form">{{ trans('quote.info') }}</label>--}}
                                        {{--<textarea rows="5" class="form-control"></textarea>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<input type="hidden" name="lang" id="lang" value="{{\App::getLocale()}}">--}}
                                {{--<input value="{{ trans('home.btn-text') }}" class="btn btn-primary btn-lg" type="submit">--}}
                                {{--{{ csrf_field() }}--}}
                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
        {{--<input class="btn btn-primary mt10" type="submit" value="Search for Flights" />--}}
    {{--</form>--}}
    <div class="gap gap-small"></div>

    </div>
    <div class="gap"></div>
{{--</div>--}}

@endsection





<script>
//    $('#oneway').hasClass('active')
 $('#flight-search-2').on('click', function(){
     console.log('oneway');
 });



    var placeSearch, autocomplete, autocomplete2;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {

        var southWest = new google.maps.LatLng(34.5428, -31.4647999);
        var northEast = new google.maps.LatLng(82.1673907, 74.3555001);
        var myCustomEuropeBounds = new google.maps.LatLngBounds(southWest, northEast);
        var options = {
            bounds: myCustomEuropeBounds,
            strictBounds: true,
            types: ['(cities)']
        };

        function autocomplete() {
            new google.maps.places.Autocomplete(document.getElementById('autocomplete'), options);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                fillInAddress();
            });
        }

        function autocomplete2() {
            new google.maps.places.Autocomplete(document.getElementById('autocomplete2'), options);

            google.maps.event.addListener(autocomplete2, 'place_changed', function () {
                fillInAddress();
            });
        }

        autocomplete();
        autocomplete2();
    }
    function fillInAddress() {
        console.log('test2');
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var place2 = autocomplete2.getPlace();
        console.log(place);
        console.log(place2);
        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        for (var i = 0; i < place2.address_components.length; i++) {
            var addressType2 = place2.address_components[i].types[0];
            if (componentForm[addressType2]) {
                var val2 = place2.address_components[i][componentForm[addressType2]];
                document.getElementById(addressType2).value = val2;
            }
        }
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }

        console.log('test');

    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        console.log('test1');
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                var start = autocomplete.setBounds(circle.getBounds());
                var end = autocomplete2.setBounds(circle.getBounds());
                console.log(start);
                console.log(end);


            });
        }

    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7dU1rftKPndLThKPGhAKPfvzg1dvCp8E&libraries=places&callback=initAutocomplete"
        async defer></script>