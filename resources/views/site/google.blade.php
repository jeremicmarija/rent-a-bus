<html>
<head>
    <style type="text/css">
        body {
            font-family: sans-serif;
            font-size: 14px;
        }
    </style>

    <title>Google Maps JavaScript API v3 Example: Places Autocomplete</title>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIi5mxltiwKtZO688t1E8fSL40Y6LS6gM&libraries=places" async defer></script>
    {{--<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places" type="text/javascript"></script>--}}
    <script type="text/javascript">
//        var countries = ['es','al','ad','at','by','be','ba','bg','hr','cy','cz','dk','ee','fo','fi','fr','de','gi','gr','hu','is','ie','im','it','lv','li','lt','lu','mk','mt','md','mc','me','nl','no','pl','pt','ro','ru','sm','rs','sk','si','se','ch','ua','gb','va'];
//        function initialize() {
//
//            var options = {
//                types: ['(cities)'],
//                componentRestrictions:  {'country': countries}
//
//        };
//
//            var input = document.getElementById('searchTextField');
//            var autocomplete = new google.maps.places.Autocomplete(input, options);
//        }

var myCustomEuropeBounds =  {
                "northeast" : {
                    "lat" : 82.1673907,
                        "lng" : 74.3555001
                },
                "southwest" : {
                    "lat" : 34.5428,
                        "lng" : -31.4647999
                }
            };

function initialize() {
    var options = {
        bounds: myCustomEuropeBounds,
        strictBounds: true,
        types: ['(cities)']
    };

    new google.maps.places.Autocomplete(document.getElementById('autocomplete'), options);
}
//       new google.maps.event.addDomListener(window, 'load', initialize);
    </script>

</head>
<body>
<div>
    <input id="autocomplete" type="text" size="50" placeholder="Enter a location" autocomplete="on">

    {{--<input id="searchTextField" type="text" size="50" placeholder="Enter a location" autocomplete="on">--}}
</div>
</body>
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIi5mxltiwKtZO688t1E8fSL40Y6LS6gM&libraries=places&callback=initAutocomplete"--}}
        {{--async defer></script>--}}
</html>




