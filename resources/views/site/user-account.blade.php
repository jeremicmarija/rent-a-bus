@extends('site.layouts.app')

@section('title')
    User account
@endsection

@section('content')

    <div class="container">
        <h3 class="booking-title">{{trans('account.your_acc')}}</h3>
        <div class="row">
            <div class="col-md-3">
                <aside class="user-profile-sidebar">
                    <div class="user-profile-avatar text-center">
                        <div class="img-container">
                            <img src="{{ asset('assets/site/images/users/' . Auth::user()->photo) }}" alt="Image Alternative text" title="{{Auth::user()->first_name . " " . Auth::user()->last_name}}" />
                            <span class="img-remove"><i class="fa fa-trash-o" aria-hidden="true"></i> {{trans('account.remove')}}</span>
                            <input type="hidden" id="userId" value="{{Auth::user()->id}}">
                        </div>
                        <h5>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h5>
                        <p>{{trans('account.member')}} {{ Auth::user()->created_at->diffForHumans() }}</p>
                    </div>
                    <ul class="list user-profile-nav">
                        {{--<li><a href="{{ route('user.account') }}"><i class="fa fa-user"></i>{{trans('account.a_details')}}</a></li>--}}
                        {{--<li><a href="{{ route('user.rent.history') }}"><i class="fa fa-clock-o"></i>{{trans('account.r_history')}}</a></li>--}}
                    </ul>
                </aside>
            </div>
            <div class="col-md-9">
                <div class="row">
                    @if(session('user_updated'))
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissible text-center feedback" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ session('user_updated') }}
                            </div>
                        </div>
                    @endif
                    @if(session('user_not_updated'))
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissible text-center feedback" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ session('user_not_updated') }}
                            </div>
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{--<form action="{{ route('user.account.update') }}" id="account" method="post" enctype="multipart/form-data">--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<h4>{{trans('account.a_details')}}</h4>--}}
                            {{--<div class="form-group form-group-icon-left {{ $errors->has('first_name') ? 'has-error' : '' }}"><i class="fa fa-user input-icon"></i>--}}
                                {{--<label>{{trans('register.f_name')}}</label>--}}
                                {{--<input name="first_name" class="form-control" value="{{ Auth::user()->first_name }}" type="text" />--}}
                                {{--@if ($errors->has('first_name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('first_name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="form-group form-group-icon-left {{ $errors->has('last_name') ? 'has-error' : '' }}"><i class="fa fa-user input-icon"></i>--}}
                                {{--<label>{{trans('register.l_name')}}</label>--}}
                                {{--<input name="last_name" class="form-control" value="{{ Auth::user()->last_name }}" type="text" />--}}
                                {{--@if ($errors->has('last_name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('last_name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="form-group form-group-icon-left {{ $errors->has('email') ? 'has-error' : '' }}"><i class="fa fa-envelope input-icon"></i>--}}
                                {{--<label>E-mail</label>--}}
                                {{--<input name="email" class="form-control" value="{{ Auth::user()->email }}" type="text" />--}}
                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="form-group form-group-icon-left {{ $errors->has('birth_date') ? 'has-error' : '' }}"><i class="fa fa-clock-o input-icon"></i>--}}
                                {{--<label>{{trans('register.b_date')}}</label>--}}
                                {{--<input id="userBD" type="hidden" value="{{ date('d m, Y', strtotime(Auth::user()->birth_date)) }}">--}}
                                {{--<input id="birth_date" type="text" value="{{date('d m, Y', strtotime(Auth::user()->birth_date))}}" class="date-pick form-control" placeholder="" name="birth_date" readonly="true" data-date-format="dd/mm/yyyy"/>--}}
                                {{--@if ($errors->has('birth_date'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('birth_date') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="form-group form-group-icon-left {{ $errors->has('driving_licence_number') ? 'has-error' : '' }}"><i class="fa fa-car input-icon"></i>--}}
                                {{--<label>{{trans('register.licence')}}</label>--}}
                                {{--<input name="driving_licence_number" class="form-control" value="{{ Auth::user()->driving_licence_number }}" type="text" />--}}
                                {{--@if ($errors->has('driving_licence_number'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('driving_licence_number') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="form-group form-group-icon-left {{ $errors->has('password') ? 'has-error' : '' }} {{ session('incorrect_password') ? 'has-error' : '' }}"><i class="fa fa-lock input-icon"></i>--}}
                                {{--<label>{{trans('account.curr_pass')}}</label>--}}
                                {{--<input name="password" class="form-control" type="password" />--}}
                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                                {{--@if (session('incorrect_password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ session('incorrect_password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon"></i>--}}
                                {{--<label>{{trans('account.new_pass')}}</label>--}}
                                {{--<input name="new_password" class="form-control" type="password" />--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label>{{trans('account.photo')}}</label>--}}
                                {{--<input type="file" name="photo" />--}}
                            {{--</div>--}}
                            {{--<hr>--}}
                            {{--<input type="submit" class="btn btn-primary" value="{{trans('account.save')}}">--}}
                            {{--<div class="gap gap-small"></div>--}}
                        {{--</form>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('assets/site/js/inputmask.js')}}"></script>
    <script>
        $('.date-pick').mask('99/99/9999',{placeholder:"mm/dd/yyyy"});
    </script>
@stop