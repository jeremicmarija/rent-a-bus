<!DOCTYPE HTML>
<html>
<head>
    @include('site.includes.head')
</head>

<body>
    @include('site.includes.header')

    @yield('content')
    @include('site.includes.footer')
    @include('site.includes.scripts')
    @yield('scripts')

</body>

</html>