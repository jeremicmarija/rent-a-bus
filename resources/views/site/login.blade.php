@extends('site.layouts.app')

@section('content')
    <div class="gap"></div>
    <div class="container">
        <div class="row h400" data-gutter="60">
            <div class="col-md-5 col-md-offset-1">
                <h1 class="text-center booking-title">{{trans('home.login')}}</h1>
                <form class="form-horizontal" id="login_form" role="form" method="POST" action="{{ url('/login?lang='. App::getLocale()) }}">
                    {{ csrf_field() }}
                    <div class="form-group form-group-icon-left {{ $errors->has('email') ? ' has-error' : '' }}"><i class="fa fa-user input-icon input-icon-show"></i>
                        <label for="email">Email</label>
                        <input id="email" type="email" value="{{ old('email') }}" class="form-control" placeholder="" name="email" />
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group form-group-icon-left {{ $errors->has('password') ? ' has-error' : '' }}"><i class="fa fa-lock input-icon input-icon-show"></i>
                        <label for="password">{{trans('login.pass')}}</label>
                        <input id="password" class="form-control" type="password" placeholder="" name="password" />
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="checkbox rememberMe">
                            <span>
                                <input class="i-check" type="checkbox" name="remember"> {{trans('login.remember')}}
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" value="{{trans('home.login')}}" />
                    </div>
                    <div class="form-group">
                        <a id="forgot_link" class="pull-left" href="{{ url('/password/reset') }}">{{trans('login.forgot')}}</a>
                        <a class="pull-right" href="{{ route('register') }}">{{trans('login.account')}}</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="gap"></div>
    </div>
@endsection