@extends('site.layouts.app')

@section('title')
    Fudeks | Busevi
@stop

@section('content')
    <div class="gap"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <aside class="booking-filters text-white">
                    <h3>Filter By:</h3>
                    <ul class="list booking-filters-list">
                        <li>
                            <h5 class="booking-filters-title">Number seats</h5>
                            <div class="checkbox">
                                <label>
                                    <input class="i-check" type="checkbox" />0-28 ()</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="i-check" type="checkbox" />29-48 ()</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="i-check" type="checkbox" />49-57  ()</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="i-check" type="checkbox" />58-70 ()</label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="i-check" type="checkbox" />71+  ()</label>
                            </div>
                        </li>
                        {{--<li>--}}
                            {{--<h5 class="booking-filters-title">Attractions</h5>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Perfomances (126)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Cultural (80)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Museums (130)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Landmarks (52)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Outdoors (62)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Amusement (22)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Sports (32)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Zoos & Aquariums (7)</label>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<h5 class="booking-filters-title">Activities</h5>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Sightseeing Tours (184)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Wellness & Spas (130)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Food & Drink (40)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Adventure (15)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Classes (34)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Gear Rentals (10)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Ranch & Farm (1)</label>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<h5 class="booking-filters-title">Nightlife</h5>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Bars (115)</label>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input class="i-check" type="checkbox" />Clubs (63)</label>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    </ul>
                </aside>
            </div>
            <div class="col-md-9">
                <div class="nav-drop booking-sort">
                    <h5 class="booking-sort-title"><a href="#">Sort: Ranking<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
                    <ul class="nav-drop-menu">
                        <li><a href="#">Name (A-Z)</a>
                        </li>
                        <li><a href="#">Name (Z-A)</a>
                        </li>
                        <li><a href="#">Number of Reviews</a>
                        </li>
                        <li><a href="#">Just Added</a>
                        </li>
                    </ul>
                </div>
                <div class="row row-wrap">
                    @if(count($buses)>0)
                        @foreach($buses as $bus)
                            <?php
                            $pictures = json_decode($bus->pictures, true);
                            $picture = $pictures[0];
                            ?>
                            <div class="col-md-4">
                                <a href="{{ route('bus',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr', 'bus-id'=>$bus->id, 'params'=>implode('-',explode(' ',$bus->name))]) }}">
                                    <div class="thumb">
                                        <header class="thumb-header">
                                            <a class="hover-img" href="{{ route('bus',['lang'=>App::getLocale() == 'en' ? 'en' : 'sr', 'bus-id'=>$bus->id, 'params'=>implode('-',explode(' ',$bus->name))]) }}">
                                                <img src="{{asset('/assets/site/images/buses/' . $picture)}}" alt="{{$bus->name}}">
                                                <h5 class="hover-title-center">{{$bus->name}}</h5>
                                            </a>
                                        </header>
                                        <div class="thumb-caption">
                                            <h5 class="thumb-title"><a class="text-darken" href="#">Manhattan Skyline</a></h5>
                                            <p class="mb0"><small><i class="fa fa-map-marker"></i> New York, NY (Midtown East)</small>
                                            </p>
                                            <p class="mb0 text-darken"><span class="text-lg lh1em text-color"><small >from</small> Free</span>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                       @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="gap"></div>
    </div>
@endsection