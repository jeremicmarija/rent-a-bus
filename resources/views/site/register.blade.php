{{--@extends('site.layouts.app')--}}

{{--@section('title')--}}
    {{--Fudeks | {{trans('home.register')}}--}}
{{--@stop--}}

{{--@section('content')--}}
    {{--<div class="gap"></div>--}}
    {{--<div class="container">--}}
        {{--<div class="col-md-6 col-md-offset-3">--}}
            {{--@if(session('reg_mess'))--}}
                {{--<div class="alert alert-danger alert-dismissible text-center feedback" role="alert">--}}
                    {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                    {{--{{ session('reg_mess') }}--}}
                {{--</div>--}}
            {{--@endif--}}
        {{--</div>--}}
        {{--<div class="row" data-gutter="60">--}}
            {{--<div class="col-md-5 col-md-offset-1">--}}
                {{--<h1 class="text-center booking-title">{{trans('register.register')}}</h1>--}}
                {{--<form class="form-horizontal" id="register_form" role="form" method="POST" action="{{ url('/register?lang=' . App::getLocale()) }}">--}}
                    {{--{{ csrf_field() }}--}}
                    {{--<div class="form-group form-group-icon-left {{ $errors->has('first_name') ? ' has-error' : '' }}"><i class="fa fa-user input-icon input-icon-show"></i>--}}
                        {{--<label for="first_name">{{trans('register.f_name')}}</label>--}}
                        {{--<input id="first_name" type="text" value="{{ session('first_name') ? session('first_name') : old('first_name') }}" class="form-control" placeholder="" name="first_name" />--}}
                        {{--@if ($errors->has('first_name'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('first_name') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="form-group form-group-icon-left {{ $errors->has('last_name') ? ' has-error' : '' }}"><i class="fa fa-user input-icon input-icon-show"></i>--}}
                        {{--<label for="last_name">{{trans('register.l_name')}}</label>--}}
                        {{--<input id="last_name" type="text" value="{{ session('last_name') ? session('last_name') : old('last_name') }}" class="form-control" placeholder="" name="last_name" />--}}
                        {{--@if ($errors->has('last_name'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('last_name') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="form-group form-group-icon-left {{ $errors->has('birth_date') ? ' has-error' : '' }}"><i class="fa fa-user input-icon input-icon-show"></i>--}}
                        {{--<label for="birth_date">{{trans('register.b_date')}}</label>--}}
                        {{--<input id="birth_date" type="text" value="{{ old('birth_date') }}" class="date-pick form-control" placeholder="" name="birth_date" />--}}
                        {{--@if ($errors->has('birth_date'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('birth_date') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="form-group form-group-icon-left {{ $errors->has('email') ? ' has-error' : '' }}"><i class="fa fa-user input-icon input-icon-show"></i>--}}
                        {{--<label for="email">Email</label>--}}
                        {{--<input id="email" type="email" value="{{ session('email') ? session('email') : old('email') }}" class="form-control" placeholder="" name="email" />--}}
                        {{--@if ($errors->has('email'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('email') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="form-group form-group-icon-left {{ $errors->has('password') ? ' has-error' : '' }}"><i class="fa fa-lock input-icon input-icon-show"></i>--}}
                        {{--<label for="password">{{trans('register.pass')}}</label>--}}
                        {{--<input id="password" class="form-control" type="password" placeholder="" name="password" />--}}
                        {{--@if ($errors->has('password'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('password') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="form-group form-group-icon-left {{ $errors->has('password_confirmation') ? ' has-error' : '' }}"><i class="fa fa-lock input-icon input-icon-show"></i>--}}
                        {{--<label for="password_confirmation">{{trans('register.con_pass')}}</label>--}}
                        {{--<input id="password_confirmation" class="form-control" type="password" placeholder="" name="password_confirmation" />--}}
                        {{--@if ($errors->has('password_confirmation'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('password_confirmation') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="form-group form-group-icon-left {{ $errors->has('driving_licence') ? ' has-error' : '' }}"><i class="fa fa-user input-icon input-icon-show"></i>--}}
                        {{--<label for="driving_licence">{{trans('register.licence')}}</label>--}}
                        {{--<input id="driving_licence" type="driving_licence" value="{{ old('driving_licence') }}" class="form-control" placeholder="" name="driving_licence" />--}}
                        {{--@if ($errors->has('driving_licence'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('driving_licence') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<input class="btn btn-primary" type="submit" value="{{trans('register.register')}}" />--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
            {{--<div class="col-md-4 col-md-offset-1">--}}
                {{--<h1 class="text-center booking-title">{{trans('register.reg_with')}}</h1>--}}
                {{--<div class="row social_login">--}}
                    {{--<div class="col-xs-12 social-btn">--}}
                        {{--<a href="{{ route('facebook.login') }}" class="btn btn-primary fb-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-12 text-center social-btn">--}}
                        {{--<span class="sep">{{trans('login.or')}}</span>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-12 social-btn">--}}
                        {{--<a href="{{ route('google.login') }}" class="btn btn-primary gplus-button"><i class="fa fa-google-plus" aria-hidden="true"></i> Google+</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="gap"></div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@stop--}}