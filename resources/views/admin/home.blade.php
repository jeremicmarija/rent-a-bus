@extends('admin.layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header text-center">
         <h1>
             Dashboard
         </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="fa fa-car" aria-hidden="true"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">All cars</span>
                        {{--<span class="info-box-number">{{count($cars)}}</span>--}}
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <!-- /.col -->
            <div class="col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-car" aria-hidden="true"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Currently active cars</span>
                        {{--<span class="info-box-number">{{count($active_cars)}}</span>--}}
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Registered users</span>
                        {{--<span class="info-box-number">{{count($users)}}</span>--}}
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Number of reservations</span>
                        {{--<span class="info-box-number">{{ count($reservations) }}</span>--}}
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <!-- fix for small devices only -->
            {{--<div class="clearfix visible-sm-block"></div>--}}
        </div>
        <!-- /.row -->

        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="box">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">Latest Reservations</h3>--}}
                        {{--<div class="box-tools pull-right">--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                            {{--</button>--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="col-md-8">--}}
                            {{--<!-- TABLE: LATEST RESERVATIONS -->--}}
                            {{--<div class="table-responsive">--}}
                                {{--<table class="table no-margin">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>Reservation ID</th>--}}
                                        {{--<th>Car</th>--}}
                                        {{--<th>Pick up Date</th>--}}
                                        {{--<th>Drop off Date</th>--}}
                                        {{--<th>Total cost</th>--}}
                                        {{--<th>Payment status</th>--}}
                                        {{--<th>Status</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}
                                    {{--@if(count($latest_reservations)>0)--}}
                                        {{--@foreach($latest_reservations as $reservation)--}}
                                            {{--<tr>--}}
                                                {{--<td>{{ $reservation->id }}</td>--}}
                                                {{--<td>{{ $reservation->car->brand . " " . $reservation->car->model }}</td>--}}
                                                {{--<td>{{ $reservation->pick_up_date }}</td>--}}
                                                {{--<td>{{ $reservation->drop_off_date }}</td>--}}
                                                {{--<td>{{ $reservation->total_cost }} &euro;</td>--}}
                                                {{--<td>{{($reservation->paid) ? 'Paid' : 'Not paid'}}</td>--}}
                                                {{--@if($reservation->paid == 1 && $reservation->status == 1)--}}
                                                    {{--<td class=" text-success">Reserved</td>--}}
                                                {{--@elseif($reservation->paid == 1 && $reservation->status == 2)--}}
                                                    {{--<td class=" text-success">Finished</td>--}}
                                                {{--@elseif($reservation->paid == 0 && $reservation->status == 1)--}}
                                                    {{--<td class=" text-success">On Hold</td>--}}
                                                {{--@elseif($reservation->paid == 0 && $reservation->status == 0)--}}
                                                    {{--<td class=" text-danger">Canceled</td>--}}
                                                {{--@endif--}}
                                            {{--</tr>--}}
                                        {{--@endforeach--}}
                                    {{--@endif--}}
                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--</div>--}}
                            {{--<!-- /.table-responsive -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                        {{--<div class="col-md-4">--}}
                            {{--<p class="text-center">--}}
                                {{--<strong>Reservation Stats</strong>--}}
                            {{--</p>--}}

                            {{--<div class="progress-group">--}}
                                {{--<span class="progress-text">Finished Reservations</span>--}}
                                {{--<span class="progress-number"><b>{{ count($reservations->where('status','2')) }}</b>/{{ count($reservations) }}</span>--}}
                                {{--<?php--}}
                                    {{--if(count($reservations)>0){--}}
                                        {{--$percent = (count($reservations->where('status','2'))/count($reservations))*100 . '%';--}}
                                    {{--}else{--}}
                                        {{--$percent = '0%';--}}
                                    {{--}--}}
                                {{--?>--}}
                                {{--<div class="progress sm">--}}
                                    {{--<div class="progress-bar progress-bar-aqua" style="width: {{ $percent }}"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.progress-group -->--}}
                            {{--<div class="progress-group">--}}
                                {{--<span class="progress-text">Canceled Reservations</span>--}}
                                {{--<span class="progress-number"><b>{{ count($reservations->where('status','0')) }}</b>/{{ count($reservations) }}</span>--}}
                                {{--<?php--}}
                                {{--if(count($reservations)>0){--}}
                                    {{--$percent = (count($reservations->where('status','0'))/count($reservations))*100 . '%';--}}
                                {{--}else{--}}
                                    {{--$percent = '0%';--}}
                                {{--}--}}
                                {{--?>--}}
                                {{--<div class="progress sm">--}}
                                    {{--<div class="progress-bar progress-bar-red" style="width: {{ $percent }}"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.progress-group -->--}}
                            {{--<div class="progress-group">--}}
                                {{--<span class="progress-text">Active Reservations</span>--}}
                                {{--<span class="progress-number"><b>{{ count($reservations->where('status','1')) }}</b>/{{ count($reservations) }}</span>--}}
                                {{--<?php--}}
                                {{--if(count($reservations)>0){--}}
                                    {{--$percent = (count($reservations->where('status','1'))/count($reservations))*100 . '%';--}}
                                {{--}else{--}}
                                    {{--$percent = '0%';--}}
                                {{--}--}}
                                {{--?>--}}
                                {{--<div class="progress sm">--}}
                                    {{--<div class="progress-bar progress-bar-green" style="width: {{ $percent }}"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.progress-group -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                    {{--</div>--}}
                    {{--<!-- /.box-body -->--}}
                    {{--<div class="box-footer text-center">--}}
                        {{--<a href="{{ route('admin.reservations') }}" class="uppercase">View All Reservations</a>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-footer -->--}}
                {{--</div>--}}
                {{--<!-- ./box -->--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}
        {{--</div>--}}
        <!-- /.row -->
        {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="box">--}}
                {{--<div class="box-header with-border">--}}
                    {{--<h3 class="box-title">Expired Reservations</h3>--}}
                    {{--<div class="box-tools pull-right">--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                        {{--</button>--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>--}}
                        {{--</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="box-body">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<!-- TABLE: LATEST RESERVATIONS -->--}}
                        {{--<div class="table-responsive">--}}
                            {{--<table class="table no-margin">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Reservation ID</th>--}}
                                    {{--<th>Car</th>--}}
                                    {{--<th>Pick up Date</th>--}}
                                    {{--<th>Drop off Date</th>--}}
                                    {{--<th>Total cost</th>--}}
                                    {{--<th>Payment status</th>--}}
                                    {{--<th>Status</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--@if(count($expired_reservations)>0)--}}
                                    {{--@foreach($expired_reservations as $exp_reservation)--}}
                                        {{--<tr class="expired">--}}
                                            {{--<td>{{ $exp_reservation->id }}</td>--}}
                                            {{--<td>{{ $exp_reservation->car->brand . " " . $exp_reservation->car->model }}</td>--}}
                                            {{--<td>{{ $exp_reservation->pick_up_date }}</td>--}}
                                            {{--<td>{{ $exp_reservation->drop_off_date }}</td>--}}
                                            {{--<td>{{ $exp_reservation->total_cost }} &euro;</td>--}}
                                            {{--<td>{{($exp_reservation->paid) ? 'Paid' : 'Not paid'}}</td>--}}
                                            {{--<td>Expired</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                {{--@endif--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}
                        {{--<!-- /.table-responsive -->--}}
                    {{--</div>--}}
                    {{--<!-- /.col -->--}}
                {{--</div>--}}
                {{--<!-- /.box-body -->--}}
                {{--<div class="box-footer text-center">--}}
                    {{--<a href="{{ route('admin.reservations') }}" class="uppercase">View All Reservations</a>--}}
                {{--</div>--}}
                {{--<!-- /.box-footer -->--}}
            {{--</div>--}}
            {{--<!-- ./box -->--}}
        {{--</div>--}}
        {{--<!-- /.col -->--}}
        {{--</div> --}}{{-- end Row--}}

        <!-- Main row -->
        {{--<div class="row">--}}
            {{--<!-- Left col -->--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<!-- USERS LIST -->--}}
                        {{--<div class="box box-danger">--}}
                            {{--<div class="box-header with-border">--}}
                                {{--<h3 class="box-title">Latest Members</h3>--}}
                                {{--<div class="box-tools pull-right">--}}
                                    {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                                    {{--</button>--}}
                                    {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.box-header -->--}}
                            {{--<div class="box-body no-padding">--}}
                                {{--<ul class="users-list clearfix">--}}
                                    {{--@if(count($latest_users)>0)--}}
                                        {{--@foreach($latest_users as $user)--}}
                                            {{--<li>--}}
                                                {{--<div class="admin-user-img">--}}
                                                    {{--<img class="circle-image" src="{{ asset('assets/site/images/users/' . $user->photo) }}" alt="User Image">--}}
                                                {{--</div>--}}
                                                {{--<a class="users-list-name" href="{{ route('admin.users') }}">{{ $user->first_name }} {{ $user->last_name }}</a>--}}
                                                {{--<span class="users-list-date">{{ $user->created_at->diffForHumans() }}</span>--}}
                                            {{--</li>--}}
                                        {{--@endforeach--}}
                                    {{--@endif--}}
                                {{--</ul>--}}
                                {{--<!-- /.users-list -->--}}
                            {{--</div>--}}
                            {{--<!-- /.box-body -->--}}
                            {{--<div class="box-footer text-center">--}}
                                {{--<a href="{{ route('admin.users') }}">View All Users</a>--}}
                            {{--</div>--}}
                            {{--<!-- /.box-footer -->--}}
                        {{--</div>--}}
                        {{--<!--/.box -->--}}
                    {{--</div>--}}
                    {{--<!-- /.col -->--}}
                    {{--<div class="col-md-6">--}}
                        {{--<!-- Cars LIST -->--}}
                        {{--<div class="box box-primary">--}}
                            {{--<div class="box-header with-border">--}}
                                {{--<h3 class="box-title">Recently Added Cars</h3>--}}
                                {{--<div class="box-tools pull-right">--}}
                                    {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                                    {{--</button>--}}
                                    {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.box-header -->--}}
                            {{--<div class="box-body">--}}
                                {{--<ul class="products-list product-list-in-box">--}}
                                    {{--@if(count($latest_cars))--}}
                                        {{--@foreach($latest_cars as $car)--}}
                                            {{--<li class="item">--}}
                                                {{--@if($car->pictures != '[]')--}}
                                                    {{--<?php--}}
                                                    {{--$pictures = json_decode($car->pictures,true);--}}
                                                    {{--?>--}}
                                                {{--@endif--}}
                                                {{--<div class="product-img">--}}
                                                    {{--<img src="{{ asset('assets/site/images/cars/' . $pictures[0]) }}" alt="">--}}
                                                {{--</div>--}}
                                                {{--<div class="product-info">--}}
                                                    {{--<a href="{{ route('admin.cars') }}" class="product-title">{{ $car->brand }} {{ $car->model }}--}}
                                                        {{--<span class="label label-warning pull-right">{{ $car->category->name }}</span>--}}
                                                    {{--</a>--}}
                                                    {{--<span class="product-description">--}}
                                                        {{--{{ $car->description }}--}}
                                                    {{--</span>--}}
                                                {{--</div>--}}
                                            {{--</li>--}}
                                        {{--@endforeach--}}
                                    {{--@endif--}}
                                    {{--<!-- /.item -->--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<!-- /.box-body -->--}}
                            {{--<div class="box-footer text-center">--}}
                                {{--<a href="{{ route('admin.cars') }}" class="uppercase">View All Cars</a>--}}
                            {{--</div>--}}
                            {{--<!-- /.box-footer -->--}}
                        {{--</div>--}}
                        {{--<!-- /.box -->--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- /.row -->--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}
        {{--</div>--}}
        {{--<!-- /.row -->--}}
    </section>
    <!-- /.content -->
@stop
