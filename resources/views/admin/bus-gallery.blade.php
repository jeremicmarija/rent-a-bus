@extends('admin.layouts.app')

@section('title')
    {{$bus->name}} Gallery
@stop

@section('content')
    <div id="bus_gallery">
        <section class="content-header text-center">
            <h1>
                {{$bus->name}} Gallery
            </h1>
        </section>

        <div class="row">
            <div class="col-md-12">
                <div class="row" id="thumbnail-row">
                    @if(count($bus_pictures)>0)
                        @foreach($bus_pictures as $picture)
                            <div class="col-md-3 text-center gallery-image">
                                <img class="img-responsive img-thumbnail" src="{{ asset('assets/site/images/buses/' . $picture) }}" alt="">
                                <div class="col-md-12 text-center">
                                    <button title="Remove Image" data-image="{{ $picture }}" data-href="{{route('admin-buses-gallery-remove',['bus_id'=>$bus->id])}}" class="remove-btn btn btn-danger"><i class="fa fa-times fa-2x"></i></button>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

        <div class="row" id="gallery-upload-images">
            <div class="col-md-12">
                <form>
                    <div class="form-group">
                        <label for="images">Add New Image:</label>
                        <input type="file" name="images" id="gallery-image">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Upload" id="gallery-upload-btn" class="btn btn-success">
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="gallery-token">
                    <input type="hidden" id="gallery-href" value="{{route('admin-buses-gallery-add',['bus_id'=>$bus->id])}}">
                    <input type="hidden" id="base_url" value="{{ url('/') }}">
                </form>
            </div>
        </div>
    </div>
@stop

@section('pageScript')
    <script>
        $(document).ready(function(){

            //Add image
            $('#gallery-upload-btn').on('click',function(event){
                event.preventDefault();
                var image = $('#gallery-image')[0].files[0];
                var token = $('#gallery-token').val();
                var href = $('#gallery-href').val();
                var formData = new FormData();
                formData.append('image',image);
                $.ajax({
                    'url':href,
                    'method':'POST',
                    'type':'JSON',
                    'processData': false,
                    'contentType': false,
                    'data':formData,
                    'headers':{'X-CSRF-TOKEN':token}
                }).success(function(response){
                    if(response['image'] != null){
                        var row = $('#thumbnail-row');
                        var base_url = $('#base_url').val();
                        row.append('<div class="col-md-3 text-center">' +
                            '<img class="img-responsive img-thumbnail" src="' + base_url + '/assets/site/images/buses/' + response['image'] + '" alt="">' +
                            '<div class="col-md-12 text-center">' +
                            '<button title="Remove Image" class="remove-btn btn btn-danger" data-image="' + response['image'] + '" data-href="' + base_url + '/admin/buses/gallery/remove?bus_id=' + response['bus_id'] + '"><i class="fa fa-times fa-2x"></i></button>' +
                            '</div>' +
                            '</div>');
                    }
                });
            });

            //Remove image
            $('#thumbnail-row').on('click','.remove-btn',function(){
                var image = $(this).attr('data-image');
                var url = $(this).attr('data-href');
                var token = $('#gallery-token').val();
                $.ajax({
                    'url':url,
                    'method':'POST',
                    'type':'JSON',
                    'data':{'image':image,'_token':token}
                }).success(function(response){
                    var row = $('#thumbnail-row');
                    var base_url = $('#base_url').val();
                    row.empty();
                    for(var i in response['images']){
                        row.append('<div class="col-md-3 text-center">' +
                            '<img class="img-responsive img-thumbnail" src="' + base_url + '/assets/site/images/buses/' + response['images'][i] + '" alt="">' +
                            '<div class="col-md-12 text-center">' +
                            '<button title="Remove Image" class="remove-btn btn btn-danger" data-image="' + response['images'][i] + '" data-href="' + base_url + '/admin/buses/gallery/remove?bus_id=' + response['bus_id'] + '"><i class="fa fa-times fa-2x"></i></button>' +
                            '</div>' +
                            '</div>');
                    }
                });
            });
        });
    </script>
@stop