<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    @include('admin.includes.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('admin.includes.header')
    @include('admin.includes.sidebar')
    <div class="content-wrapper">
        <div id="ajax-loader">
            <div class="loader"></div>
        </div>
        @yield('content')
    </div>
    {{--@include('admin.includes.footer')--}}
</div>
<!-- ./wrapper -->
@include('admin.includes.scripts')
@yield('pageScript')
</body>
</html>
