@extends('admin.layouts.app')

@section('title')
    Buses
@stop

@section('content')
    <div id="buses">
        <div class="row">
            @if(session('bus-created'))
                <div class="col-xs-12">
                    <div class="alert alert-info alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-created')}}
                    </div>
                </div>
            @endif
            @if(session('bus-not-created'))
                <div class="col-xs-12">
                    <div class="alert alert-danger alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-not-created')}}
                    </div>
                </div>
            @endif
            @if(session('bus-updated'))
                <div class="col-xs-12">
                    <div class="alert alert-info alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-updated')}}
                    </div>
                </div>
            @endif
            @if(session('bus-not-updated'))
                <div class="col-xs-12">
                    <div class="alert alert-danger alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-not-updated')}}
                    </div>
                </div>
            @endif
            @if(session('bus-status-changed'))
                <div class="col-xs-12">
                    <div class="alert alert-info alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-status-changed')}}
                    </div>
                </div>
            @endif
        </div>

        <section class="content-header text-center">
            <h1>
                Buses
            </h1>
        </section>

        <div class="row">
            <div class="col-xs-8 buses">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Number Seats</th>
                            <th>Details</th>
                            <th>Brand</th>
                            <th>Pictures</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Edit</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($buses)>0)
                            @foreach($buses as $bus)
                                <tr>
                                    <td>{{$bus->id}}</td>
                                    <td>{{$bus->name}}</td>
                                    <td>{{$bus->num_seats}}</td>
                                    <td>
                                        @if($bus->details)
                                            <ul>
                                                @foreach($bus->details as $detail)
                                                    <li>{{$detail->name}}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </td>
                                    <td>{{$bus->category->name }}</td>
                                    <td>
                                        @if($bus->pictures != '[]')
                                            <a href="{{route('admin-buses-gallery',['bus_id'=>$bus->id])}}">Gallery</a>
                                        @else
                                            <a href="{{route('admin-buses-gallery',['bus_id'=>$bus->id])}}">Add Pictures</a>
                                        @endif
                                    </td>
                                    <td>{{$bus->created_at->diffForHumans()}}</td>
                                    <td>{{$bus->updated_at->diffForHumans()}}</td>
                                    <td><a href="" class="btn btn-warning">Edit</a></td>
                                    <td><a href="{{route('admin-buses-status',['bus_id'=>$bus->id])}}" class="btn {{$bus->status == 1 ? 'btn-danger' : 'btn-success'}}">{{$bus->status == 1 ? 'Disable' : 'Enable'}}</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" class="text-center"><h5>No recently created buses.</h5></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 col-md-offset-4 text-center">
                    {{$buses->links()}}
                </div>
            </div>
            <div class="col-xs-3" id="right_part">
                <div class="form-group text-center">
                    <button id="create-btn" class="btn btn-info">Hide Create Form</button>
                </div>
                {!! Form::open(['method'=>'POST','url'=>route('admin-buses-create'),'id'=>'buses_create_form','files'=>true]) !!}
                <div class="form-group {{$errors->has('bus_name') ? 'has-error' : ''}}">
                    {!! Form::label('bus_name','Bus Name:') !!}
                    {!! Form::text('bus_name',old('bus_name'),['class'=>'form-control']) !!}
                    @if($errors->has('bus_name'))
                        <span class="help-block">
                                <strong>*{{ $errors->first('bus_name') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group {{$errors->has('num_seats') ? 'has-error' : ''}}">
                    {!! Form::label('num_seats','Bus number seats:') !!}
                    {!! Form::number('num_seats',old('num_seats'),['class'=>'form-control']) !!}
                    @if($errors->has('num_seats'))
                        <span class="help-block">
                                <strong>*{{ $errors->first('num_seats') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                    {!! Form::label('category_id','Bus category:') !!}
                    {!! Form::select('category_id',$categories,null,['class'=>'form-control','placeholder'=>'Choose bus brand:']) !!}
                    @if ($errors->has('category_id'))
                        <span class="help-block">
                            <strong>*{{ $errors->first('category_id') }}</strong>
                        </span>
                    @endif
                </div>
                @if(count($details)>0)
                    <p>Details:</p>
                    @foreach($details as $detail)
                        <div class="form-group">
                            {{ Form::checkbox('detail_' . $detail->id,$detail->id, null) }}
                            <span>{{$detail->name}}</span>
                        </div>
                    @endforeach
                @endif
                <div class="form-group">
                    {!! Form::label('bus_pictures','Pictures:') !!}
                    {!! Form::file('bus_pictures[]', array('multiple'=>true)) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Create Bus',['class'=>'form-control btn btn-success']) !!}
                </div>
                {!! Form::close() !!}

                <hr>

                <div class="form-group text-center">
                    <button id="edit-btn" class="btn btn-info">Hide Edit Form</button>
                </div>
                {!! Form::open(['method'=>'POST','url'=>route('admin-buses-update'),'id'=>'buses_edit_form','files'=>true]) !!}
                <div class="form-group {{$errors->has('edit_bus_name') ? 'has-error' : ''}}">
                    {!! Form::label('edit_bus_name','New Bus Name:') !!}
                    {!! Form::text('edit_bus_name',old('edit_bus_name'),['class'=>'form-control']) !!}
                    @if($errors->has('edit_bus_name'))
                        <span class="help-block">
                                <strong>*{{ $errors->first('edit_bus_name') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group {{$errors->has('edit_num_seats') ? 'has-error' : ''}}">
                    {!! Form::label('edit_num_seats','Edit bus number seats:') !!}
                    {!! Form::number('edit_num_seats',old('edit_num_seats'),['class'=>'form-control']) !!}
                    @if($errors->has('edit_num_seats'))
                        <span class="help-block">
                                <strong>*{{ $errors->first('edit_num_seats') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                    {!! Form::label('edit_category_id','Edit bus category:') !!}
                    {!! Form::select('edit_category_id',$categories,$bus->category->id,['class'=>'form-control','placeholder'=>'Choose bus brand:']) !!}
                    @if ($errors->has('edit_category_id'))
                        <span class="help-block">
                            <strong>*{{ $errors->first('edit_category_id') }}</strong>
                        </span>
                    @endif
                </div>
                @if(count($details)>0)
                    <p>Details:</p>
                    @foreach($details as $detail)
                        <div class="form-group">
                            {{ Form::checkbox('detail_' . $detail->id,$detail->id, null) }}
                            <span>{{$detail->name}}</span>
                        </div>
                    @endforeach
                @endif
                <div class="form-group">
                    {!! Form::label('edit_bus_pictures','New Pictures:') !!}
                    {!! Form::file('edit_bus_pictures[]', array('multiple'=>true)) !!}
                </div>
                {!! Form::hidden('edit_bus_id',old('edit_bus_id')) !!}
                <div class="form-group">
                    {!! Form::submit('Update Bus',['class'=>'form-control btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="col-xs-1"></div>
        </div>
    </div>
@stop

@section('pageScript')
    <script>
        $(document).ready(function(){
            var edit_form = $('#buses_edit_form');
            var edit_btn = $('#edit-btn');
            edit_form.hide();
            edit_btn.hide();
            $('.buses table tbody tr td:nth-child(9)').on('click',function(e){
                e.preventDefault();
                var name = $(this).parents('tr').find('td:nth-child(2)').text();
                var id = $(this).parents('tr').find('td:nth-child(1)').text();
                var num_seat = $(this).parents('tr').find('td:nth-child(3)').text();
                edit_form.find('[name="edit_bus_name"]').val(name);
                edit_form.find('[name="edit_bus_id"]').val(id);
                edit_form.find('[name="edit_num_seats"]').val(num_seat);

                edit_btn.show();
                edit_form.slideDown();
                var details = $(this).parents('tr').find('td:nth-child(4) ul li');
                var details_names = [];
                details.each(function(){
                    details_names.push($(this).text());
                });
                var details_spans = edit_form.find('input[type="checkbox"]').siblings('span');
                details_spans.each(function(a){
                    var checked = 0;
                    for(var i in details_names){
                        if($(this).text() == details_names[i]){
                            checked++;
                            if(!$(this).siblings('input[type="checkbox"]')[0].checked){
                                $(this).siblings('input[type="checkbox"]')[0].checked = true;
                            }
                        }
                    }
                    if(!checked){
                        $(this).siblings('input[type="checkbox"]')[0].checked = false;
                    }
                });
            });
            edit_btn.on('click',function(){
                edit_form.slideUp();
                $(this).slideUp();
            });
            $('#right_part').on('click','#create-btn',function(){
                var create_form = $('#buses_create_form');
                if(create_form.css('display') == 'block'){
                    $(this).text('Show Create Form');
                }else{
                    $(this).text('Hide Create Form');
                }
                create_form.slideToggle();
            });
        });
    </script>
@stop