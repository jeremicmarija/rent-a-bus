<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{ asset('assets/site/css/bootstrap.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
{{--Icon moon pack--}}
<link rel="stylesheet" href="{{ asset('assets/site/css/icomoon.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('assets/site/css/jquery-jvectormap-1.2.2.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('assets/site/css/adminLTE.min.css') }}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ asset('assets/site/css/all-skins.min.css') }}">
{{--Custom styles--}}
{{--<link rel="stylesheet" href="{{ asset('assets/site/css/mystyles.css') }}">--}}
{{--Custom colors--}}
{{--<link rel="stylesheet" href="{{ asset('assets/site/css/mycolors.css') }}">--}}
{{--Sweet alert--}}
<link rel="stylesheet" href="{{ asset('assets/site/css/sweetalert.css') }}">

{{--<link rel="stylesheet" href="{{ asset('assets/site/css/styles.css') }}">--}}
<link rel="stylesheet" href="{{ asset('assets/site/css/bootstrap-datepicker.min.css') }}">
{{--<link rel="stylesheet" href="{{ asset('assets/site/css/bootstrap-datepicker.standalone.min.css') }}">--}}

{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}