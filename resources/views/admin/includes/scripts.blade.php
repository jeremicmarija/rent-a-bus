{{--<script src="{{ asset('assets/site/js/lang/translation-' . \App::getLocale() . '.js') }}"></script>--}}
<!-- jQuery 2.2.3 -->
<script src="{{ asset('assets/site/js/jquery.js') }}"></script>
{{--Date picker--}}
<script src="{{ asset('assets/site/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/site/js/bootstrap-timepicker.js') }}"></script>
{{--Custom js--}}
<script src="{{ asset('assets/site/js/myscripts.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets/site/js/bootstrap.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/site/js/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/site/js/app.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('assets/site/js/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('assets/site/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('assets/site/js/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{ asset('assets/site/js/jquery.slimscroll.min.js') }}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{ asset('assets/site/js/chart.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{ asset('assets/site/js/dashboard2.js') }}"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/site/js/demo.js') }}"></script>
{{--Sweet alert--}}
<script src="{{ asset('assets/site/js/sweetalert.min.js') }}"></script>
