<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2016 <a href="http://almsaeedstudio.com">XS Systems</a>.</strong> All rights
    reserved.
</footer>