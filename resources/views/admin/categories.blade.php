@extends('admin.layouts.app')

@section('title')
    Bus categories
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header text-center">
        <h1>
            Bus Categories
        </h1>
    </section>
    <div class="row">
        <div class="col-md-12 ">
            @if(session('category_created'))
                <div class="alert alert-success alert-dismissible text-center feedback" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ session('category_created') }}
                </div>
            @endif
            @if(session('category_not_created'))
                <div class="alert alert-warning alert-dismissible text-center feedback" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ session('category_not_created') }}
                </div>
            @endif
            @if(session('category_updated'))
                <div class="alert alert-success alert-dismissible text-center feedback" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ session('category_updated') }}
                </div>
            @endif
            @if(session('category_not_updated'))
                <div class="alert alert-warning alert-dismissible text-center feedback" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ session('category_not_updated') }}
                </div>
            @endif
            @if(session('category_status_changed'))
                <div class="alert alert-success alert-dismissible text-center feedback" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ session('category_status_changed') }}
                </div>
            @endif
            @if(session('category_status_not_changed'))
                <div class="alert alert-warning alert-dismissible text-center feedback" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ session('category_status_not_changed') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row" id="cars-cat">
        <div class="col-md-8 bus_categories">
            <div class="table-responsive">
                <table id="admin-table" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category Name</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Edit</th>
                        <th>Change status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($categories)>0)
                        @foreach($categories as $category)
                            <tr>
                                <td class="id">{{ $category->id }}</td>
                                <td class="name">{{ $category->name }}</td>
                                <td>{{ $category->created_at->diffForHumans() }}</td>
                                <td>{{ $category->updated_at->diffForHumans() }}</td>
                                <td><a href="#" class="btn btn-warning edit-btn" title="Edit"><i class="fa fa-pencil"></i></a></td>
                                <td><a href="/admin/buses/categories/{{ $category->id }}" class="btn {{($category->status) ? 'btn-danger' : 'btn-success'}} change-status" title="{{ ($category->status) ? 'Deactivate' : 'Activate' }}"><i class="{{ ($category->status) ? 'fa fa-ban' : 'fa fa-check-circle' }}"></i></a></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="col-md-6 col-md-offset-3 text-center">
                {{ $categories->links() }}
            </div>
        </div>

        <div class="col-xs-3" id="right_part">
            <div class="form-group text-center">
                <button id="create-btn" class="btn btn-info">Hide Create Form</button>
            </div>
            {!! Form::open(['method'=>'POST','url'=>route('admin-bus-category-create'),'id'=>'bus_categories_create_form']) !!}
            <div class="form-group {{$errors->has('detail_name') ? 'has-error' : ''}}">
                {!! Form::label('name','Category Name:') !!}
                {!! Form::text('name',old('name'),['class'=>'form-control']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        <strong>*{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                {!! Form::submit('Create Category',['class'=>'form-control btn btn-success']) !!}
            </div>
            {!! Form::close() !!}

            <hr>

            <div class="form-group text-center">
                <button id="edit-btn" class="btn btn-info">Hide Edit Form</button>
            </div>
            {!! Form::open(['method'=>'POST','url'=>route('admin-bus-category-update'),'id'=>'bus_categories_edit_form']) !!}
            <div class="form-group {{$errors->has('edit_detail_name') ? 'has-error' : ''}}">
                {!! Form::label('edit_category_name','New Category Name:') !!}
                {!! Form::text('edit_category_name',old('edit_category_name'),['class'=>'form-control']) !!}
                @if($errors->has('edit_category_name'))
                    <span class="help-block">
                            <strong>*{{ $errors->first('edit_category_name') }}</strong>
                        </span>
                @endif
            </div>
            {!! Form::hidden('edit_category_id',old('edit_category_id')) !!}
            <div class="form-group">
                {!! Form::submit('Update Category',['class'=>'form-control btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-xs-1"></div>
    </div>
    </div>
@stop

@section('pageScript')
    <script>
        $(document).ready(function(){

            var edit_form = $('#bus_categories_edit_form');
            var edit_btn = $('#edit-btn');
            edit_form.hide();
            edit_btn.hide();
            $('.bus_categories table tbody tr td:nth-child(5)').on('click',function(e){
                e.preventDefault();
                console.log('test');
                var name = $(this).parents('tr').find('td:nth-child(2)').text();
                var id = $(this).parents('tr').find('td:nth-child(1)').text();
                edit_form.find('[name="edit_category_name"]').val(name);
                edit_form.find('[name="edit_category_id"]').val(id);
                edit_btn.show();
                edit_form.slideDown();
            });
            edit_btn.on('click',function(){
                console.log('click');
                edit_form.slideUp();
                $(this).slideUp();
            });
            $('#right_part').on('click','#create-btn',function(){
                var create_form = $('#bus_categories_create_form');
                if(create_form.css('display') == 'block'){
                    $(this).text('Show Create Form');
                }else{
                    $(this).text('Hide Create Form');
                }
                create_form.slideToggle();
            });
        });
    </script>
@stop