@extends('admin.layouts.app')

@section('title')
    Bus's Details
@stop

@section('content')
    <div id="bus_details">
        <div class="row">
            @if(session('bus-detail-saved'))
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="alert alert-info alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-detail-saved')}}
                    </div>
                </div>
            @endif
            @if(session('bus-detail-not-saved'))
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="alert alert-danger alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-detail-not-saved')}}
                    </div>
                </div>
            @endif
            @if(session('bus-detail-updated'))
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="alert alert-info alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-detail-updated')}}
                    </div>
                </div>
            @endif
            @if(session('bus-detail-not-updated'))
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="alert alert-danger alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-detail-not-updated')}}
                    </div>
                </div>
            @endif
            @if(session('bus-detail-status-changed'))
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="alert alert-info alert-dismissible text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{session('bus-detail-status-changed')}}
                    </div>
                </div>
            @endif
        </div>

        <section class="content-header text-center">
            <h1>
                Bus's Details
            </h1>
        </section>
        <div class="row">
            <div class="col-xs-8 bus_details">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Translated Name</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Edit</th>
                            <th>Change Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($details))
                            @foreach($details as $detail)
                                <tr>
                                    <td>{{$detail->id}}</td>
                                    <td>{{$detail->name}}</td>
                                    <td>{{$detail->translate->sr}}</td>
                                    <td>{{$detail->created_at->diffForHumans()}}</td>
                                    <td>{{$detail->updated_at->diffForHumans()}}</td>
                                    <td><a href="" class="btn btn-warning">Edit</a></td>
                                    <td><a href="{{route('admin-bus-detail-status',['detail_id'=>$detail->id])}}" class="btn {{$detail->status == 1 ? 'btn-danger' : 'btn-success'}}">{{$detail->status == 1 ? 'Disable' : 'Enable'}}</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7" class="text-center"><h5>No recently created details.</h5></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 col-md-offset-4 text-center">
                    {{$details->links()}}
                </div>
            </div>
            <div class="col-xs-3" id="right_part">
                <div class="form-group text-center">
                    <button id="create-btn" class="btn btn-info">Hide Create Form</button>
                </div>
                {!! Form::open(['method'=>'POST','url'=>route('admin-bus-detail-create'),'id'=>'bus_details_create_form']) !!}
                <div class="form-group {{$errors->has('detail_name') ? 'has-error' : ''}}">
                    {!! Form::label('detail_name','Detail Name:') !!}
                    {!! Form::text('detail_name',old('detail_name'),['class'=>'form-control']) !!}
                    @if($errors->has('detail_name'))
                        <span class="help-block">
                            <strong>*{{ $errors->first('detail_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{$errors->has('detail_translated_name') ? 'has-error' : ''}}">
                    {!! Form::label('detail_translated_name','Detail Translated Name:') !!}
                    {!! Form::text('detail_translated_name',old('detail_translated_name'),['class'=>'form-control']) !!}
                    @if($errors->has('detail_translated_name'))
                        <span class="help-block">
                                <strong>*{{ $errors->first('detail_translated_name') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::submit('Create Detail',['class'=>'form-control btn btn-success']) !!}
                </div>
                {!! Form::close() !!}

                <hr>

                <div class="form-group text-center">
                    <button id="edit-btn" class="btn btn-info">Hide Edit Form</button>
                </div>
                {!! Form::open(['method'=>'POST','url'=>route('admin-bus-detail-update'),'id'=>'bus_details_edit_form']) !!}
                <div class="form-group {{$errors->has('edit_detail_name') ? 'has-error' : ''}}">
                    {!! Form::label('edit_detail_name','New Detail Name:') !!}
                    {!! Form::text('edit_detail_name',old('edit_detail_name'),['class'=>'form-control']) !!}
                    @if($errors->has('edit_detail_name'))
                        <span class="help-block">
                            <strong>*{{ $errors->first('edit_detail_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{$errors->has('edit_detail_translated_name') ? 'has-error' : ''}}">
                    {!! Form::label('edit_detail_translated_name','New Detail Translated Name:') !!}
                    {!! Form::text('edit_detail_translated_name',old('edit_detail_translated_name'),['class'=>'form-control']) !!}
                    @if($errors->has('edit_detail_translated_name'))
                        <span class="help-block">
                            <strong>*{{ $errors->first('edit_detail_translated_name') }}</strong>
                        </span>
                    @endif
                </div>
                {!! Form::hidden('edit_detail_id',old('edit_detail_id')) !!}
                <div class="form-group">
                    {!! Form::submit('Update Detail',['class'=>'form-control btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="col-xs-1"></div>
        </div>
    </div>
@stop

@section('pageScript')
    <script>
        $(document).ready(function(){
            console.log('test');
            var edit_form = $('#bus_details_edit_form');
            var edit_btn = $('#edit-btn');
            edit_form.hide();
            edit_btn.hide();
            $('.bus_details table tbody tr td:nth-child(6)').on('click',function(e){
                e.preventDefault();
                var name = $(this).parents('tr').find('td:nth-child(2)').text();
                var trans_name = $(this).parents('tr').find('td:nth-child(3)').text();
                var id = $(this).parents('tr').find('td:nth-child(1)').text();
                edit_form.find('[name="edit_detail_name"]').val(name);
                edit_form.find('[name="edit_detail_translated_name"]').val(trans_name);
                edit_form.find('[name="edit_detail_id"]').val(id);
                edit_btn.show();
                edit_form.slideDown();
            });
            edit_btn.on('click',function(){
                console.log('click');
                edit_form.slideUp();
                $(this).slideUp();
            });
            $('#right_part').on('click','#create-btn',function(){
                var create_form = $('#bus_details_create_form');
                if(create_form.css('display') == 'block'){
                    $(this).text('Show Create Form');
                }else{
                    $(this).text('Hide Create Form');
                }
                create_form.slideToggle();
            });
        });
    </script>
@stop