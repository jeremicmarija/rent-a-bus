@extends('site.layouts.app')

@section('custom_seo')
    <title>Fudeks | Send Reset Password Email</title>
    @if(App::getLocale() == 'en')
    <meta name="keywords" content="Fudeks, rent a car, rental, Belgrade, airport, renting, pick up, vehicle" />
    <meta name="description" content="Iznajmite vaš savršen automobil - Fudeks rent a car Beograd."/>
        @else
        <meta name="keywords" content="Fudeks, rent a car, aerodrom, Beograd, rentiranje, automobili, najam, vozila" />
        <meta name="description" content="Fudeks rent a car - rent your perfect car in Belgrade"/>
    @endif
@stop

@section('content')
    <div class="gap"></div>
    <div class="container">
        <div class="row" data-gutter="60">
            <div class="col-md-4 col-md-offset-4">
                <h3 class="text-center">Reset Password Email</h3>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                    {{ csrf_field() }}
                    <div class="form-group form-group-icon-left {{ $errors->has('email') ? ' has-error' : '' }}"><i class="fa fa-user input-icon input-icon-show"></i>
                        <label for="email">Email</label>
                        <input id="email" type="email" value="{{ old('email') }}" class="form-control" placeholder="" name="email" />
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" value="Send Reset Password Link" />
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
