<?php

return [
    'journey-type' => 'Vrsta putovanja (opcionalno)',
    'luggage' => 'Prtljag (opcionalno)',
    'pick-date' => 'Datum i vreme preuzimanja',
    'drop-date' => 'Datum i vreme vraćanja',
    'company' => 'Ime kompanije (opcionalno)',
    'contactfn' => 'Kontakt ime',
    'contactln' => 'Kontakt prezime',
    'phone' => 'Broj telefona',
    'mail' => 'E-mail adresa',
    'info' => 'Dodatne informacije',
];