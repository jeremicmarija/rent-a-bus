<?php

return [
    'your_acc'=>'Vaš nalog',
    'remove'=>'Izbriši',
    'a_details'=>'Izmeni podatke',
    'r_history'=>'Arhiva',
    'curr_pass'=>'Trenutna šifra',
    'new_pass'=>'Nova šifra',
    'save'=>'Zapamti promene',
    'photo'=>'Slika',
    'member'=>'Registrovan od pre',
    'car'=>'Automobil',
    'total'=>'Ukupna cena',
    'change'=>'Promeni',
    'reserved'=>'Rezervisan',
    'finished'=>'Završena',
    'canceled'=>'Otkazana',
    'no_res'=>'Nema skorijih rezervacija',
    'hold'=>'Na čekanju'
];