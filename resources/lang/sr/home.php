<?php
return [
    'account'=>'Profil',
    'logout'=>'Odjavi se',
    'login'=>'Uloguj se',
    'register'=>'Registracija',
    'lang'=>'Jezik',
    'home'=>'Početna',
    'cars'=>'Automobili',
    'about'=>'O nama',
    'contact'=>'Kontakt',
    'quote' => 'Posalji upit',
    'hi'=>'Zdravo',
    'title' => 'Posalji upit',
    'btn-text' => 'Posalji upit odmah',
    'slide-h1' => 'Zelite da rentirate bus ?',
    'pick-loc' => 'Početna lokacija',
    'drop-loc' => 'Krajnja lokacija',
    'num-pass' => 'Broj putnika',
    'ride-type' => 'Tip vozila',
    'buses' => 'Autobusi',
    'h_about' => 'Svi naši autobusi su visokopodni, visoke turističke kategorije, ne stariji od 10 godina, klimatizovani, potpuno opremljeni TV, video i audio opremom, sa toaletom i friziderom. Pored značajnog broja osnovnih sredstava preduzeće raspolaže i izuzetnim prihvatno-otpremnim centrom koji se nalazi na samom autoputu Beograd – Niš.',

];