<?php

return [
    'register'=>'registracija',
    'login'=>'uloguj-se',
    'pass'=>'sifra',
    'reset'=>'resetovanje',
    'search'=>'pretraga',
    'about'=>'rent-a-bus-beograd',
    'offers'=>'ponude',
    'contact'=>'kontakt',
    'customer'=>'vozac',
    'info'=>'informacije',
    'checkout'=>'placanje',
    'success'=>'uspesno',
    'user'=>'korisnik',
    'account'=>'nalog',
    'rent'=>'iznajmljivanje',
    'history'=>'arhiva',
    'product'=>'product',
    'terms-and-conditions'=>'uslovi-najma',
    'quote' => 'upit',
    'buses' => 'autobusi',
    'bus' => 'autobus'
];