<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Imejl adresa ili šifra koju ste uneli se ne poklapaju ni sa jednim nalogom.',
    'throttle' => 'Previše neuspešnih pokušaja. Molimo Vas pokušajte kasnije za :seconds seconds.',

];
