<?php

return [
    'register'=>'Registruj se',
    'f_name'=>'Ime',
    'l_name'=>'Prezime',
    'b_date'=>'Datum rođenja',
    'pass'=>'Šifra',
    'con_pass'=>'Potvrdite šifru',
    'licence'=>'Broj vozačke dozvole',
    'reg_with'=>'Registruj se pomoću'
];