<?php

return [
    'contact'=>'Kontaktirajte nas',
    'mess'=>'Kontaktirajte nas putem telefona ili email-a i rezervišite vaš autobus još danas!',
    'name'=>'Ime',
    'mess1'=>'Poruka',
    'send'=>'Pošalji poruku',
    'phone'=>'Broj telefona',
    'address'=>'Adresa',
    'serbia'=>'Srbija',
    'text_mess'=>'Unesite vašu poruku ovde',
    'contact_tab'=>'Kontaktirajte nas',
    'bg'=>'Beograd',
    'add'=>'ulaz iz Prešernove'
];