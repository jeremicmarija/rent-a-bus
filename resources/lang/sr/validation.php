<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute mora biti prihvaćen.',
    'active_url'           => 'The :attribute URL adresa nije validna.',
    'after'                => 'The :attribute mora biti datum posle :date.',
    'alpha'                => 'The :attribute može sadržati samo slova.',
    'alpha_dash'           => 'The :attribute može sadržati samo slova, brojeve, i crte.',
    'alpha_num'            => 'The :attribute može sadržati samo slova i brojeve.',
    'array'                => 'The :attribute mora biti niz.',
    'before'               => 'The :attribute mora biti datum pre :date.',
    'between'              => [
        'numeric' => 'The :attribute mora biti između :min i :max.',
        'file'    => 'The :attribute mora biti između :min i :max kilobajta.',
        'string'  => 'The :attribute mora biti između :min i :max karaktera.',
        'array'   => 'The :attribute mora biti između :min i :max stavki.',
    ],
    'boolean'              => 'The :attribute polje mora biti tačno ili pogrešno.',
    'confirmed'            => 'The :attribute potvrda ne odgovara.',
    'date'                 => 'The :attribute nije validnog datuma.',
    'date_format'          => 'The :attribute ne odgovara formatu :format.',
    'different'            => 'The :attribute i :other moraju biti drugačiji.',
    'digits'               => 'The :attribute mora biti :digits u ciframa.',
    'digits_between'       => 'The :attribute mora biti između :min i :max cifara.',
    'distinct'             => 'The :attribute polja imaju dupliranu vrednost.',
    'email'                => 'The :attribute mora biti validna email adresa.',
    'exists'               => 'The odabran :attribute je nevažeći.',
    'filled'               => 'The :attribute polje je obavezno.',
    'image'                => 'The :attribute mora biti slika.',
    'in'                   => 'The odabrani :attribute je nevažeći.',
    'in_array'             => 'The :attribute polje ne postoji u :other.',
    'integer'              => 'The :attribute mora biti ceo broj.',
    'ip'                   => 'The :attribute mora biti validna IP adresa.',
    'json'                 => 'The :attribute mora biti validan JSON niz.',
    'max'                  => [
        'numeric' => 'The :attribute ne može biti veći od :max.',
        'file'    => 'The :attribute ne može biti veći od :max kilobajta.',
        'string'  => 'The :attribute ne može biti veći od :max karaktera.',
        'array'   => 'The :attribute ne može imati viće od :max stavki.',
    ],
    'mimes'                => 'The :attribute mora biti datoteka tipa: :values.',
    'min'                  => [
        'numeric' => 'The :attribute mora biti najmanje :min.',
        'file'    => 'The :attribute mora biti najmanje :min kilobajta.',
        'string'  => 'The :attribute mora biti najmanje :min karaktera.',
        'array'   => 'The :attribute mora biti najmanje :min stavki.',
    ],
    'not_in'               => 'The odabrani :attribute je nevažeći.',
    'numeric'              => 'The :attribute mora biti broj.',
    'present'              => 'The :attribute polje mora biti prisutno .',
    'regex'                => 'The :attribute format je nevažeći.',
    'required'             => 'Polje :attribute je obavezno.',
    'required_if'          => 'The :attribute polje je obavezno kada :other je :value.',
    'required_unless'      => 'The :attribute polje je obavezno osim :other je u :values.',
    'required_with'        => 'The :attribute polje je obavezno kada :values je prisutno.',
    'required_with_all'    => 'The :attribute polje je obavezno kada :values je prisutno.',
    'required_without'     => 'The :attribute polje je obavezno kada :values nije prisutno.',
    'required_without_all' => 'The :attribute polje je obavezno kada nijedan od :values nije prisutan.',
    'same'                 => 'The :attribute i :other moraju se podudarati.',
    'size'                 => [
        'numeric' => 'The :attribute mora biti :size.',
        'file'    => 'The :attribute mora biti :size kilobajti.',
        'string'  => 'The :attribute mora biti:size karakteri.',
        'array'   => 'The :attribute mora sadržati :size stavke.',
    ],
    'string'               => 'The :attribute mora biti niz.',
    'timezone'             => 'The :attribute mora biti validna zona.',
    'unique'               => 'The :attribute je već zauzeto.',
    'url'                  => 'The :attribute format je nevažeći.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];