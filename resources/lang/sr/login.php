<?php

return [
    'remember'=>'Zapamti me',
    'forgot'=>'Zaboravili ste šifru?',
    'account'=>'Nemate nalog?',
    'login_with'=>'Ulogujte se pomoću',
    'or'=>'ili',
    'pass'=>'Šifra'
];