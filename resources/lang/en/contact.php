<?php

return [
    'contact'=>'Contact Us',
    'mess'=>'Contact us via phone or email and book your wish bus today!',
    'name'=>'Name',
    'mess1'=>'Message',
    'send'=>'Send Message',
    'phone'=>'Phone Number',
    'address'=>'Address',
    'serbia'=>'Serbia',
    'text_mess'=>'Type your message here',
    'contact_tab'=>'Contact us',
    'bg'=>'Belgrade',
    'add'=>'entrance from Prešernova'
];