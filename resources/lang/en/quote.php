<?php

return [
   'journey-type' => 'Journey type (optional)',
    'luggage' => 'Luggage (optional)',
    'pick-date' => 'Pickup date & time',
    'drop-date' => 'Return date & time',
    'company' => 'Company name (optional)',
    'contactfn' => 'Contact first name',
    'contactln' => 'Contact last name',
    'phone' => 'Phone number',
    'mail' => 'E-mail address',
    'info' => 'Additional information',

];