<?php

return [
    'your_acc'=>'Your Account',
    'remove'=>'Remove',
    'a_details'=>'Account Details',
    'r_history'=>'Renting History',
    'curr_pass'=>'Current Password',
    'new_pass'=>'New Password',
    'save'=>'Save Changes',
    'photo'=>'Photo',
    'member'=>'Member Since',
    'car'=>'Car',
    'total'=>'Total Cost',
    'change'=>'Change',
    'reserved'=>'Reserved',
    'finished'=>'Finished',
    'canceled'=>'Canceled',
    'no_res'=>'No reservation',
    'hold'=>'On Hold'
];