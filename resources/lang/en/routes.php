<?php

return [
    'register'=>'register',
    'login'=>'login',
    'pass'=>'password',
    'reset'=>'reset',
    'search'=>'search',
    'about'=>'about',
    'offers'=>'offers',
    'contact'=>'contact',
    'customer'=>'customer',
    'info'=>'info',
    'checkout'=>'checkout',
    'success'=>'success',
    'user'=>'user',
    'account'=>'account',
    'rent'=>'rent',
    'history'=>'history',
    'terms-and-conditions'=>'terms-and-conditions',
    'registracija'=>'register',
    'uloguj-se'=>'login',
    'sifra'=>'password',
    'resetovanje'=>'reset',
    'pretraga'=>'search',
    'rent-a-bus-beograd'=>'about',
    'ponude'=>'offers',
    'kontakt'=>'contact',
    'automobil'=>'car',
    'vozac'=>'customer',
    'informacije'=>'info',
    'placanje'=>'checkout',
    'uspešno'=>'success',
    'korisnik'=>'user',
    'nalog'=>'account',
    'iznajmljivanje'=>'rent',
    'arhiva'=>'history',
    'limuzine'=>'limousine',
    'product'=>'product',
    'uslovi-najma'=>'terms-and-conditions',
    'quote' => 'quote',
    'upit' => 'quote',
    'buses' => 'buses',
    'autobusi' => 'buses',
    'bus' => 'bus',
    'autobus' => 'bus'

];