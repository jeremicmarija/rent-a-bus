<?php

return [
    'register'=>'Register',
    'f_name'=>'First name',
    'l_name'=>'Last name',
    'b_date'=>'Birth date',
    'pass'=>'Password',
    'con_pass'=>'Confirm password',
    'licence'=>'Driving licence number',
    'reg_with'=>'Register with'
];