<?php

return [
    'account'=>'Account',
    'logout'=>'Logout',
    'login'=>'Login',
    'register'=>'Register',
    'lang'=>'Language',
    'home'=>'Home',
    'cars'=>'Cars',
    'about'=>'About us',
    'contact'=>'Contact',
    'quote' => 'Get a quote',
    'hi'=>'Hi',
    'title' => 'Request a quote',
    'btn-text' => 'Request quote now',
    'slide-h1' => 'Need to Rent A Bus ?',
    'pick-loc' => 'Pickup location',
    'drop-loc' => 'Destionation',
    'num-pass' => 'Number of passengers',
    'ride-type' => 'Ride type',
    'buses' => 'Buses',
    'h_about' => 'All our buses are high-speed, high-tourist categories, not older than 10 years, air-conditioned, fully equipped TV, video and audio equipment, with toilet and refrigerator. In addition to a significant number of fixed assets, the company also has an excellent reception and dispatch center located on the highway Belgrade - Nis.',

];