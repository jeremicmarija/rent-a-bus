<?php

return [
    'remember'=>'Remember Me',
    'forgot'=>'Forgot Your Password?',
    'account'=>'Don\'t have an account?',
    'login_with'=>'Login with',
    'or'=>'or',
    'pass'=>'Password'
];