<?php
/**
 * Created by PhpStorm.
 * User: XS System
 * Date: 7/26/2017
 * Time: 1:21 PM
 */

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create();
    }
}
