<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bus_id')->unsigned()->nullable();
            $table->string('pick_up_loc');
            $table->date('pick_up_date');
            $table->string('pick_up_time');
            $table->string('drop_off_loc')->nullable();
            $table->date('drop_off_date')->nullable();
            $table->string('drop_off_time')->nullable();
            $table->integer('num_pass');
            $table->string('ride_type')->nullable();
            $table->string('jorney_type')->nullable();
            $table->string('luggage')->nullable();
            $table->string('company_name')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('email');
            $table->text('info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
