<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'status'];

    public function bus(){
        return $this->hasMany('App\Bus','category_id');
    }

}
