<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTranslation extends Model
{
      protected $fillable = ['detail_id','sr'];

    public function detail(){
        return $this->belongsTo('\App\Detail','detail_id');
    }
}
