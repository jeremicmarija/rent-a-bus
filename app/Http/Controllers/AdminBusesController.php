<?php

namespace App\Http\Controllers;

use App\Bus;
use App\Category;
use App\Detail;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminBusesController extends Controller
{
    public function buses()
    {
        $details = Detail::where('status', '1')->get();
        $buses = Bus::paginate(10);
        $categories = Category::where('status','1')->lists('name','id');
        return view('admin.buses', compact('details', 'buses', 'categories'));
    }

    public function create(Request $request)
    {
        $this->validate($request, ['bus_name' => 'required|max:255']);
        $bus = new Bus();
        $bus_name = $request->get('bus_name');
        $bus_num_seats = $request->get('num_seats');
        $category_id = $request->get('category_id');
        if($request->get('year_production')){
            $year_production = $request->get('year_production');
            $bus->year_production = $year_production;
        }
        $bus->name = $bus_name;
        $bus->num_seats =  $bus_num_seats;
        $bus->category_id = $category_id;
        $images = array();
        if ( $request->file('bus_pictures') )
        {
            $pictures = $request->file('bus_pictures');
            foreach ( $pictures as $picture )
            {
                if ( $picture != null )
                {
                    $images[] = $picture->getClientOriginalName() . '_' . time();
                    $picture->move(base_path('/public/assets/site/images/buses/'), $picture->getClientOriginalName() . '_' . time());
                }
            }
            $images_encoded = json_encode($images, true);
            $bus->pictures = $images_encoded;
        }
        if ( $bus->save() )
        {
            $request->session()->flash('bus-created', 'Bus ' . $bus->name . ' has been successfully created!');
        } else
        {
            $request->session()->flash('bus-not-created', 'Bus ' . $bus->name . ' has not been created!');
        }
        $details_number = count(Detail::where('status', '1')->get());
        for ( $i = 1; $i <= $details_number; $i++ )
        {
            if ( $request->get('detail_' . $i) )
            {
                $detail = Detail::findOrFail($request->get('detail_' . $i));
                $bus->details()->save($detail);
            }
        }

        return redirect()->back();
    }


    public function update(Request $request)
    {
        $this->validate($request, ['edit_bus_name' => 'required|max:255']);
        $bus_id = $request->get('edit_bus_id');
        $bus = Bus::findOrFail($bus_id);
        $bus_name = $request->get('edit_bus_name');
        $bus->name = $bus_name;
        if ( $request->file('edit_bus_pictures') && $request->file('edit_bus_pictures')[0] != null )
        {
            //Deleting old pictures from stock
            $old_pictures = json_decode($bus->pictures, true);
            foreach ( $old_pictures as $old_picture )
            {
                unlink(env('DOCUMENT_ROOT') . '/assets/site/images/buses/' . $old_picture);
            }
            //End deleting old pictures from stock
            $pictures = $request->file('edit_bus_pictures');
            foreach ( $pictures as $picture )
            {
                $images[] = $picture->getClientOriginalName() . '_' . time();
                $picture->move(base_path('/public/assets/site/images/buses/'), $picture->getClientOriginalName() . '_' . time());
            }
            $images_encoded = json_encode($images, true);
            $bus->pictures = $images_encoded;
        }
        if ( $bus->save() )
        {
            $request->session()->flash('bus-updated', 'Bus ' . $bus->name . ' has been successfully updated!');
        } else
        {
            $request->session()->flash('bus-not-updated', 'Bus ' . $bus->name . ' has not been updated!');
        }

        if($request->get('edit_year_production')){
            $year_production = $request->get('edit_year_production');
            $bus->year_production = $year_production;
        }
        $bus->num_pass = $request->get('edit_num_pass');
        //Attaching new and detaching old details related to bus
        $details_number = count(Detail::all());
        $bus_details_array = array();
        $new_bus_details_array = array();
        foreach ( $bus->details as $detail )
        {
            $bus_details_array[] = $detail->id;
        }
        for ( $i = 1; $i <= $details_number; $i++ )
        {
            if ( $request->get('detail_' . $i) )
            {
                $new_bus_details_array[] = $request->get('detail_' . $i);
            }
        }
        if ( array_diff($new_bus_details_array, $bus_details_array) )
        {
            $added_details = array_diff($new_bus_details_array, $bus_details_array);
            foreach ( $added_details as $added_d )
            {
                $bus->details()->attach($added_d);
            }
            if ( array_diff($bus_details_array, $new_bus_details_array) )
            {
                $removed_details = array_diff($bus_details_array, $new_bus_details_array);
                foreach ( $removed_details as $removed_d )
                {
                    $bus->details()->detach($removed_d);
                }
            }
        } elseif ( array_diff($bus_details_array, $new_bus_details_array) )
        {
            $removed_details = array_diff($bus_details_array, $new_bus_details_array);
            foreach ( $removed_details as $removed_d )
            {
                $bus->details()->detach($removed_d);
            }
            if ( array_diff($new_bus_details_array, $bus_details_array) )
            {
                $added_details = array_diff($new_bus_details_array, $bus_details_array);
                foreach ( $added_details as $added_d )
                {
                    $bus->details()->attach($added_d);
                }
            }
        }

        //End attaching new and detaching old details related to bus
        return redirect()->back();
    }

    public function status(Request $request)
    {
        $bus = Bus::findOrFail($request->get('bus_id'));
        if ( $bus->status == 0 )
        {
            $bus->status = 1;
            if ( $bus->save() )
            {
                $request->session()->flash('bus-status-changed', 'Bus ' . $bus->name . ' has been successfully activated!');
            }
        } else
        {
            $bus->status = 0;
            if ( $bus->save() )
            {
                $request->session()->flash('bus-status-changed', 'Bus ' . $bus->name . ' has been successfully deactivated!');
            }
        }

        return redirect()->back();
    }

}
