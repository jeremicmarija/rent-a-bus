<?php

namespace App\Http\Controllers;

use App\Bus;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminBusesGalleryController extends Controller
{
    public function gallery(Request $request){
        $bus = Bus::findOrFail($request->get('bus_id'));
        $bus_pictures = json_decode($bus->pictures,true);
        return view('admin.bus-gallery',compact('bus','bus_pictures'));
    }

    public function add(Request $request){
        $bus = Bus::findOrFail($request->get('bus_id'));
        $image = null;
        if($request->file('image') != null){
            $image = $request->file('image')->getClientOriginalName() . '_' . time();
            $database_images = $bus->pictures;
            $decoded_database_images = json_decode($database_images,true);
            array_push($decoded_database_images,$image);
            $request->file('image')->move(base_path('/public/assets/site/images/buses/'),$image);
            $bus->pictures = json_encode($decoded_database_images,true);
            $bus->save();
        }
        return response()->json(['image'=>$image,'bus_id'=>$bus->id]);
    }

    public function remove(Request $request){
        $bus = Bus::findOrFail($request->get('bus_id'));
        $image = $request->get('image');
        $database_images = $bus->pictures;
        $decoded_database_images = json_decode($database_images,true);
        $new_images_array = array();
        foreach($decoded_database_images as $img){
            if($img != $image){
                $new_images_array[] = $img;
            }else{
                unlink(env('DOCUMENT_ROOT') . '/assets/site/images/buses/' . $img);
            }
        }
        $bus->pictures = json_encode($new_images_array,true);
        $bus->save();
        $images = json_decode($bus->pictures,true);
        return response()->json(['images'=>$images,'bus_id'=>$bus->id]);
    }
}
