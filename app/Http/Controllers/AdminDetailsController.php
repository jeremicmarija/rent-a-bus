<?php

namespace App\Http\Controllers;

use App\Detail;
use App\DetailTranslation;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminDetailsController extends Controller
{
    public function details(){
        $details = Detail::paginate(10);
        return view('admin.details',compact('details'));
    }

    public function create(Request $request){
        $this->validate($request,['detail_name'=>'required|max:255','detail_translated_name'=>'required|max:255']);
        $name = $request->get('detail_name');
        $trans_name = $request->get('detail_translated_name');
        $detail = new Detail();
        $detail->name = $name;
        if($detail->save()){
            $request->session()->flash('detail-saved','Detail ' . $detail->name . ' has been successfully created!');
        }else{
            $request->session()->flash('detail-not-saved','Detail ' . $detail->name . ' has not been created!');
        }
        $detail_trans = new DetailTranslation();
        $detail_trans->sr = $trans_name;
        $detail->translate()->save($detail_trans);
        return redirect()->back();
    }

    public function update(Request $request){
        $this->validate($request,['edit_detail_name'=>'required|max:255','edit_detail_translated_name'=>'required|max:255']);
        $new_name = $request->get('edit_detail_name');
        $new_trans_name = $request->get('edit_detail_translated_name');
        $detail_id = $request->get('edit_detail_id');
        $detail = Detail::findOrFail($detail_id);
        $detail->name = $new_name;
        if($detail->save()){
            $request->session()->flash('detail-updated','Detail ' . $detail->name . ' has been successfully updated!');
        }else{
            $request->session()->flash('detail-not-updated','Detail ' . $detail->name . ' has not been updated!');
        }
        $detail_trans = new DetailTranslation();
        $detail_trans->sr = $new_trans_name;
        $detail->translate()->update(['sr'=>$new_trans_name]);
        return redirect()->back();
    }

    public function status(Request $request){
        $detail_id = $request->get('detail_id');
        $detail = Detail::findOrFail($detail_id);
        if($detail->status == 1){
            $detail->status = 0;
            if($detail->save()){
                $request->session()->flash('detail-status-changed','Detail ' . $detail->name . ' has been successfully deactivated!');
            }
        }else{
            $detail->status = 1;
            if($detail->save()){
                $request->session()->flash('detail-status-changed','Detail ' . $detail->name . ' has been successfully activated!');
            }
        }
        return redirect()->back();
    }
}
