<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;


class ReservationController extends Controller
{
    public function quote(){

        $pick_up_loc = '';
        $drop_off_loc = '' ;
        $num_pass = '' ;

        return view('site.quote', compact('pick_up_loc', 'drop_off_loc', 'num_pass'));

    }

    public function quoteSearch(Request $request)
    {

        $this->validate($request, ['pick_up_loc' => 'required', 'drop_off_loc' => 'required', 'num_pass' => 'required',
            'first_name', 'last_name']);

        $pick_up_loc = $request->get('pick_up_loc') ;
        $drop_off_loc = $request->get('drop_off_loc') ;
        $num_pass = $request->get('num_pass') ;

        return view('site.quote', compact('pick_up_loc', 'drop_off_loc', 'num_pass'));
    }

    public function makeReservation(Request $request)
    {

        $this->validate($request, ['pick_up_loc' => 'required', 'drop_off_loc' => 'required', 'num_pass' => 'required|numeric', 'category_bus' => 'required|max:255',
            'pick_up_date' => 'required', 'pick_up_time' => 'required', 'phone' => 'required', 'email' => 'required']);


        $pick_up_date = $request->get('pick_up_date');
        $drop_off_date = $request->get('drop_off_date');
        $reservation  = new Reservation();
        if($request->get('bus_id')){
            $reservation->bus_id = $request->get('bus_id');
        }
        $reservation->pick_up_loc =$request->get('pick_up_loc') ;
        $reservation->drop_off_loc =  $request->get('drop_off_loc') ;
        $reservation->num_pass = $request->get('num_pass') ;
        $reservation->ride_type = $request->get('category_bus');
        if($request->get('travel_reason')){
            $reservation->jorney_type = $request->get('travel_reason');
        }
        if($request->get('laguage')){
            $reservation->luggage = $request->get('laguage');
        }
        $reservation->pick_up_date =  date('Y-m-d',strtotime($pick_up_date));
        $reservation->pick_up_time = $request->get('pick_up_time');
        if($request->get('drop_off_date')){
            $reservation->drop_off_date = date('Y-m-d',strtotime($drop_off_date));
        }
        if($request->get('drop_off_time')){
            $reservation->drop_off_time = $request->get('drop_off_time');
        }
        $reservation->first_name = $request->get('first_name');
        $reservation->last_name = $request->get('last_name');
        if($request->get('company_name')){
            $reservation->company_name = $request->get('company_name');
        }
        $reservation->phone = $request->get('phone');
        $reservation->email = $request->get('email');
        if($request->get('info')){
            $reservation->info = $request->get('info');
        }

        if ( $reservation->save() )
        {
            \App::getLocale() == 'sr' ? $request->session()->flash('reservation_sent', 'Vaš zahtev je uspešno poslat.') : $request->session()->flash('reservation_sent', 'Your request has been successfully sent.');
        }
        else
        {
            \App::getLocale() == 'sr' ? $request->session()->flash('reservation_not_sent', 'Vaš zahtev nije poslat.Molimo pokusajte ponovo.') : $request->session()->flash('reservation_not_sent', 'Your request has not sent.Please try again');

            return redirect()->back();
        }

        $bus_id = $reservation->bus_id;
        $fname = $reservation->first_name;
        $lname = $reservation->last_name;
        $company_name = $reservation->company_name;
        $phone = $reservation->phone;
        $email = $reservation->email;
        $pick_up_loc = $reservation->pick_up_loc;
        $drop_off_loc = $reservation->drop_off_loc;
        $pick_up_date = $reservation->pick_up_date;
        $drop_off_date = $reservation->drop_off_date;

        $data = [
            'bus_id' => $bus_id,
            'fname' => $fname,
            'lname' => $lname,
            'company_name' => $company_name,
            'phone' => $phone,
            'email' => $email,
            'pick_up_loc' => $pick_up_loc ,
            'drop_off_loc' => $drop_off_loc,
            'pick_up_date' => $pick_up_date,
            'drop_off_date' => $drop_off_date,
            'pick_up_time' => $reservation->pick_up_time,
            'drop_off_time' => $reservation->drop_off_time,
            'num_pass' => $reservation->num_pass,
            'ride_type' => $reservation->ride_type,
            'jorney_type' => $reservation->jorney_type,
            'luggage' => $reservation->luggage,
            'info' =>$reservation->info,

        ];


        Mail::send('site.email.reservation', $data, function ($m) use ($data)
        {
            $m->from(env('RENT_A_BUS_EMAIL'));
            $m->to(env('RENT_A_BUS_EMAIL'));
            $m->replyTo($data['email']);
            $m->subject('Rezervacija');
        });


        \App::getLocale() == 'sr' ? $request->session()->flash('request_sent', 'Vaš zahtev je uspešno poslat.') : $request->session()->flash('request_sent', 'Your request has been successfully sent.');

        return redirect()->back();

//        return view('site.success', compact('pick_up_loc', 'drop_off_loc', 'num_pass'));
    }

}
