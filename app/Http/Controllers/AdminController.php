<?php
/**
 * Created by PhpStorm.
 * User: XS System
 * Date: 8/3/2017
 * Time: 1:13 PM
 */

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;
class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home()
    {
        return view('admin.home');
    }

    public function reservations()
    {
        $reservations = Reservation::all();

        return view('admin.reservations', compact('reservations'));
    }
}