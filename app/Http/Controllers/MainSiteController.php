<?php
/**
 * Created by PhpStorm.
 * User: XS System
 * Date: 7/25/2017
 * Time: 11:56 AM
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class MainSiteController extends Controller
{
    public function home()
    {
        return view('site.home');
    }

    public function login(Request $request){
        return view('site.login');
    }

    public function register(Request $request){
        return view('site.register');
    }

    public function about(){
        return view('site.about');
    }

    public function contact(){
        return view('site.contact');
    }

    public function userAccount(){
        return view('site.user-account');
    }

    public function google(){
        return view('site.google');
    }


}