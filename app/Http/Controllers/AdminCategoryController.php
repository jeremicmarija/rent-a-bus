<?php
/**
 * Created by PhpStorm.
 * User: XS System
 * Date: 8/8/2017
 * Time: 2:52 PM
 */

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;


class AdminCategoryController extends Controller
{
    public function categories()
    {
        $categories = Category::where('status', '1')->paginate(10);

        return view('admin.categories', compact('categories'));
    }

    public function create(Request $request)
    {
        $this->validate($request, ['name' => 'required|max:255']);
        $category = new Category();
        $category->name = $request->get('name');

        if ( $category->save() )
        {
            $request->session()->flash('category_created', 'Category ' . $category->name . ' has been successfully created!');
        } else
        {
            $request->session()->flash('category_not_created', 'Category ' . $category->name . ' has not been created!');
        }
        return redirect()->back();
    }


    public function update(Request $request){
        $this->validate($request,['edit_category_name'=>'required|max:255']);
        $new_name = $request->get('edit_category_name');
        $category_id = $request->get('edit_category_id');
        $category = Category::findOrFail($category_id);
        $category->name = $new_name;
        if($category->save()){
            $request->session()->flash('category_updated','Category ' . $category->name . ' has been successfully updated!');
        }else{
            $request->session()->flash('category_not_updated','Detail ' . $category->name . ' has not been updated!');
        }
        return redirect()->back();
    }

    public function status(Request $request){
        $category_id = $request->get('category_id');
        $category = Category::findOrFail($category_id);
        if($category->status == 1){
            $category->status = 0;
            if($category->save()){
                $request->session()->flash('category_status_changed','Detail ' . $category->name . ' has been successfully deactivated!');
            }
        }else{
            $category->status = 1;
            if($category->save()){
                $request->session()->flash('category_status_not_changed','Detail ' . $category->name . ' has been successfully activated!');
            }
        }
        return redirect()->back();
    }

}