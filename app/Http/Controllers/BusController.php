<?php
/**
 * Created by PhpStorm.
 * User: XS System
 * Date: 8/3/2017
 * Time: 3:00 PM
 */

namespace App\Http\Controllers;


use App\Bus;

class BusController extends Controller
{
    public function buses()
    {

        $buses = Bus::where('status', '1')->paginate(9);
        $num_seats_28 =  Bus::where('num_seats', '<=' ,'28')->get();

        return view('site.buses', compact('buses', 'num_seats_28'));
    }


    public function busSingle($id)
    {
        $bus = Bus::where('id', $id)->get();

        return view('site.bus', compact('bus'));
    }
}