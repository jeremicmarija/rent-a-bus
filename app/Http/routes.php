<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//home route

Route::auth();

Route::get('/', 'MainSiteController@home')->name('home');

Route::get('/admin' , 'MainSiteController@login')->name('login');

Route::get('/' . trans('routes.register'), 'MainSiteController@register')->name('register');

Route::get('/' . trans('routes.pass') . '/' . trans('routes.reset'), 'MainSiteController@resetEmail')->name('password.reset');

Route::get('/' . trans('routes.about'), 'MainSiteController@about')->name('about');

Route::get('/' . trans('routes.buses'), 'BusController@buses')->name('buses');

Route::get('/' . trans('routes.bus') . '/{params}', 'BusController@busSingle')->where('params', '^[a-zA-Z0-9-]+$')->name('bus');

Route::get('/' . trans('routes.contact'), 'MainSiteController@contact')->name('contact');

Route::get('/' . trans('routes.quote'), 'ReservationController@quote')->name('quote');

Route::get('/' . trans('routes.quote') . '/' . trans('routes.quote'), 'ReservationController@quoteSearch')->name('quote.search');

Route::post('/' . trans('routes.success'), 'ReservationController@makeReservation')->name('reservation');

Route::get('/google' , 'MainSiteController@google')->name('google');


Route::group(['middleware'=>'auth'],function(){

    Route::get('/' . trans('routes.user') . '/' . trans('routes.account'),'MainSiteController@userAccount')->name('user.account');


});



Route::group(['middleware'=>'admin'],function() {

    Route::get('/admin/home', 'AdminController@home')->name('admin.home');

//Admin bus's details
    Route::get('/admin/bus/details', 'AdminDetailsController@details')->name('admin-bus-details');

    Route::post('/admin/bus/detail/create', 'AdminDetailsController@create')->name('admin-bus-detail-create');

    Route::post('/admin/bus/detail/update', 'AdminDetailsController@update')->name('admin-bus-detail-update');

    Route::get('/admin/bus/detail/status', 'AdminDetailsController@status')->name('admin-bus-detail-status');

//Admin buses
    Route::get('/admin/buses', 'AdminBusesController@buses')->name('admin-buses');

    Route::post('/admin/buses/create', 'AdminBusesController@create')->name('admin-buses-create');

    Route::post('/admin/buses/update', 'AdminBusesController@update')->name('admin-buses-update');

    Route::get('/admin/buses/status', 'AdminBusesController@status')->name('admin-buses-status');

//Admin buses gallery
    Route::get('/admin/buses/gallery', 'AdminBusesGalleryController@gallery')->name('admin-buses-gallery');

    Route::post('/admin/buses/gallery/add', 'AdminBusesGalleryController@add')->name('admin-buses-gallery-add');

    Route::post('/admin/buses/gallery/remove', 'AdminBusesGalleryController@remove')->name('admin-buses-gallery-remove');

//Admin bus's categories
    Route::get('/admin/bus/categories', 'AdminCategoryController@categories')->name('admin-bus-categories');

    Route::post('/admin/bus/categories/create', 'AdminCategoryController@create')->name('admin-bus-category-create');

    Route::post('/admin/bus/categories/update', 'AdminCategoryController@update')->name('admin-bus-category-update');

    Route::get('/admin/bus/categories/status', 'AdminCategoryController@status')->name('admin-bus-category-status');

});

