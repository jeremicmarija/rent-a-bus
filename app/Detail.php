<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $fillable = ['name', 'status'];

    public function translate()
    {
        return $this->hasOne('\App\DetailTranslation', 'detail_id');
    }

    public function buses()
    {
        return $this->belongsToMany('\App\Bus', 'details', 'bus_id', 'detail_id');
    }
}
