<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['bus_id','pick_up_loc','pick_up_date','pick_up_time','drop_off_loc','drop_off_date',
        'drop_off_time','num_pass','ride_type','jorney_type','luggage', 'company_name', 'first_name', 'last_name', 'phone', 'email'];

    public function bus(){
        return $this->belongsTo('App\Bus','bus_id');
    }
}
