<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusesDetail extends Model
{
    protected $fillable = ['bus_id','detail_id'];

}
