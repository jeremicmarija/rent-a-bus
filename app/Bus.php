<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    protected $fillable = ['name', 'num_seats', 'year_production', 'pictures', 'status'];

    public function details()
    {
        return $this->belongsToMany('\App\Detail', 'buses_details', 'bus_id', 'detail_id');
    }

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }
}
