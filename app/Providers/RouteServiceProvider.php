<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\App;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Request $request,Router $router)
    {
        $locale = $request->get('lang');
        if($locale != 'en'){
            $this->app->setLocale('sr');
        }else{
            $this->app->setLocale('en');
        }
        if($request->get('trans')){
            $segments = $request->segments();
            $new_segments = array();
            foreach($segments as $segment){
                if(trans('routes.' . $segment) != 'routes.' . $segment){
                    $segment = trans('routes.' . $segment);
                }
                array_push($new_segments,$segment);
            }
            $new_url ='/' . implode('/',$new_segments) . '?lang=' . $this->app->getLocale();
            if($request->get('params')){
                $all_params = explode('?',$request->fullUrl())[1];
                $single_params = explode('&',$all_params);
                $new_params_arr = array();
                foreach($single_params as $param_pair){
                    if(explode('=',$param_pair)[0] != 'lang' && explode('=',$param_pair)[0] != 'params' && explode('=',$param_pair)[0] != 'trans'){
                        array_push($new_params_arr,$param_pair);
                    }
                }
                $new_params = implode('&',$new_params_arr);
                $new_url = '/' . implode('/',$new_segments) . '?' . $new_params . '&lang=' . $this->app->getLocale();
            }
            return redirect($new_url)->send();
        }
        $this->mapWebRoutes($router);
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}