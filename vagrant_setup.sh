#!/usr/bin/env bash

# **************************
# SERVER CONFIGURATION
#
MYSQL_PASSWORD='root'
DATABASE_NAME='rentabus'
NODE_VERSION='6.11.1'
NPM_VERSION='5.0.3'
#***************************

# Updating and upgrading
echo "============================================"
echo "=====> TASK #1 UBUNTU UPDATE."
echo "============================================"
sudo apt-get update -y
echo " "
echo "============================================"
echo "=====> UBUNTU UPDATE DONE."
echo "============================================"
echo " "
echo "============================================"
echo "=====> TASK #2 UBUNTU UPGRADE."
echo "============================================"
sudo apt-get upgrade -y
echo " "
echo "============================================"
echo "=====> UBUNTU UPGRADE DONE."
echo "============================================"

# Installing nginx
echo " "
echo "============================================"
echo "=====> TASK #3 INSTALL NGINX."
echo "============================================"
sudo apt-get install nginx -y
echo " "
echo "============================================"
echo "=====> NGINX INSTALLATION DONE."
echo "============================================"

# Configure nginx default for laravel application.
echo " "
echo "============================================"
echo "=====> TASK #4 CONFIGURE NGINX DEFAULT SITE."
echo "============================================"

echo 'server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    root /var/www/public/;
    index index.php index.html index.htm;

    server_name localhost;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /var/www/public/;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}' > /etc/nginx/sites-available/default
sudo service nginx restart
echo " "
echo "============================================"
echo "=====> NGINX CONFIGURED."
echo "============================================"

# Installing MYSQL
echo " "
echo "============================================"
echo "=====> TASK #5 INSTALL MYSQL."
echo "============================================"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password ${MYSQL_PASSWORD}"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${MYSQL_PASSWORD}"
sudo apt-get -y install mysql-server
sudo mysql_install_db
echo " "
echo "============================================"
echo "=====> MYSQL INSTALLED"
echo "============================================"

# Installing Expect
echo " "
echo "============================================"
echo "=====> TASK #6 INSTALL EXPECT"
echo "============================================"
sudo apt-get install expect -y
echo " "
echo "============================================"
echo "=====> EXPECT INSTALLED"
echo "============================================"

# Installing secure mysql
echo " "
echo "============================================"
echo "=====> TASK #7 INSTALL SECURE MYSQL."
echo "============================================"
SECURE_MYSQL=$(expect -c "
set timeout 3
spawn mysql_secure_installation
expect \"Enter current password for root (enter for none):\"
send \"$MYSQL_PASSWORD\r\"
expect \"Change the root password?\"
send \"n\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")
echo "${SECURE_MYSQL}"

echo " "
echo "============================================"
echo "=====> SECURE MYSQL INSTALLED"
echo "============================================"

# Create database.
echo " "
echo "============================================"
echo "=====> TASK #8 CREATE PROJECT DATABASE."
echo "============================================"
echo "CREATE DATABASE $DATABASE_NAME DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;" | mysql -uroot -p$MYSQL_PASSWORD
echo " "
echo "============================================"
echo "=====> DATABASE CREATED"
echo "============================================"

# Installing PHP on server.
echo " "
echo "============================================"
echo "=====> TASK #9 INSTALL PHP 5.6."
echo "============================================"
sudo apt-get install php5 php5-cli php5-fpm php5-curl mcrypt php5-mcrypt php5-mysql -y
echo "=====> PHP VERSION"
php -v
echo " "
sudo sed -i -e 's/^;cgi\.fix_pathinfo=1$/cgi.fix_pathinfo=0/g' /etc/php5/fpm/php.ini
echo " "
sudo service php5-fpm restart
echo " "
echo "============================================"
echo "=====> PHP INSTALLED"
echo "============================================"

# Install Composer
echo " "
echo "============================================"
echo "=====> TASK #10 INSTALL COMPOSER"
echo "============================================"
sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin/ --filename=composer
echo " "
echo "============================================"
echo "=====> COMPOSER INSTALLED"
echo "============================================"

# Install npm and node.js
echo " "
echo "============================================"
echo "=====> TASK #11 INSTALL NODE, NPM, GULP"
echo "============================================"
sudo apt-get install npm -y
sudo npm cache clean -f
sudo npm install -g n
sudo n $NODE_VERSION
sudo npm install npm@$NPM_VERSION -g
sudo npm install gulp-cli --global
echo " "
echo "============================================"
echo "=====> NODE INSTALLED"
echo "=====> NPM INSTALLED"
echo "============================================"

echo " "
echo "***********************************************************"
echo "=====> TASK #12 INSTALL GIT"
echo "***********************************************************"
# sudo apt-get install git-all -y
# echo "=====> GIT version"
# git --version
echo " "
echo "============================================"
echo "=====> SETUP DONE."
echo "============================================"

# Create var/www sync folder.
echo " "
echo "============================================"
echo "=====> TASK #13 CREATE SYNCHRONIZED WORKSPACE"
echo "============================================"
sudo mkdir -p /var/www/public
sudo chown -R $USER:$USER /var/www
sudo chmod -R 755 /var/www
sudo sed -i 's/sendfile on/sendfile off/g' /etc/nginx/nginx.conf
sudo service nginx restart
echo " "
echo "============================================"
echo "=====> WORKSPACE CREATED"
echo "=====> Access point: /var/www/ "
echo "============================================"

# Create SWAP File.
echo " "
echo "============================================"
echo "=====> TASK #14 CREATE SWAP FILE"
echo "============================================"
sudo fallocate -l 4G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

echo 'LABEL=cloudimg-rootfs / ext4 defaults 0 0
/swapfile none swap sw 0 0' > /etc/fstab
echo " "
echo "============================================"
echo "=====> SWAP CREATED."
echo "============================================"

cd /var/www/

# Create .env file.
echo " "
echo "============================================"
echo "=====> TASK #15 CREATE ENV FILE"
echo "============================================"
sudo cp .env.example .env
echo " "
echo "============================================"
echo "=====> ENV CREATED"
echo "============================================"

echo " "
echo "***********************************************************"
echo "=====> TASK #16 ADD PERMITION ON BOOTSTRAP CACHE"
echo "***********************************************************"
sudo chmod -R o+w storage/ bootstrap/cache
echo " "
echo "============================================"
echo "=====> SETUP DONE."
echo "============================================"

# Install composer dependencies.
echo " "
echo "============================================"
echo "=====> TASK #16 COMPOSER INSTALL DEPENDENCIES"
echo "============================================"
composer install
echo " "
echo "============================================"
echo "=====> COMPOSER INSTALLED"
echo "============================================"

php artisan key:generate
echo " "
echo "=====> Vagrant setup completed. Please Run Vagrant Halt and then Vagrant up before you start your development."
